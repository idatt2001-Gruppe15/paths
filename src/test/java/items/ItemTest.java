package items;

import no.ntnu.idatt2001.paths.actions.HealthAction;
import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.items.ItemType;
import no.ntnu.idatt2001.paths.story.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
  Item item;
  @BeforeEach
  void setUp() {
    item = new Item("Potion");
  }
  @Nested
  class ConstructorWorksAsIntended {
    @Test
    void constructorWithEnumParameterCreatesNonNullObject() {
      Item i = new Item(ItemType.SHIELD);
      assertNotNull(i);
    }
    @Test
    void constructorWithValidStringParameterCreatesNonNullObject() {
      Item i = new Item("Sword");
      assertNotNull(i);
    }
    @Test
    void constructorWithNullStringAsParameterThrowsException() {
      String s = null;
      assertThrows(IllegalArgumentException.class, () -> new Item(s));
    }
    @Test
    void constructorWithNullEnumAsParameterThrowsException() {
      ItemType t = null;
      assertThrows(IllegalArgumentException.class, () -> new Item(t));
    }
  }
  @Nested
  class GetMethodsReturnCorrectValues {
    @Test
    void getActionReturnsCorrectAction() {
      HealthAction expected = new HealthAction(20);
      assertEquals(expected, item.getAction());
    }
    @Test
    void getTypeReturnsCorrectType() {
      assertEquals(ItemType.POTION, item.getType());
    }
  }
  @Test
  void executeActionWorksOnPlayer() {
    Player player = new Player.Builder("John", 50).build();
    item.executeAction(player);
    assertEquals(70, player.getHealth());
  }
  @Nested
  class EqualsLogicWorksAsIntended {
    @Test
    void equalsWithSameReferenceReturnsTrue() {
      assertEquals(item, item);
    }

    @Test
    void equalsWithNullReturnsFalse() {
      assertNotEquals(null, item);
    }

    @Test
    void equalsWithTwoDefinedItemsWithSameNameReturnsTrue() {
      Item item2 = new Item("Potion");
      assertEquals(item, item2);
    }

    @Test
    void equalsWithTwoUndefinedItemsWithSameNameReturnsTrue() {
      Item rope1 = new Item("Rope");
      Item rope2 = new Item("Rope");
      assertEquals(rope1, rope2);
    }
  }
}

