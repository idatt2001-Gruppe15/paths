package fileio;

import no.ntnu.idatt2001.paths.actions.GoldAction;
import no.ntnu.idatt2001.paths.actions.HealthAction;
import no.ntnu.idatt2001.paths.actions.InventoryAction;
import no.ntnu.idatt2001.paths.actions.ScoreAction;
import no.ntnu.idatt2001.paths.fileio.StoryWriter;
import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.story.Link;
import no.ntnu.idatt2001.paths.story.Passage;
import no.ntnu.idatt2001.paths.story.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

public class StoryWriterTest {
  private Story story;
  @BeforeEach
  public void setUp() {
    Passage openingPassage = new Passage("Opening Passage", "Content for the passage");
    Link test = new Link("Some text", "Next passage");
    test.addAction(new HealthAction(5));
    test.addAction(new GoldAction(5));
    test.addAction(new InventoryAction(new Item("Sword")));
    test.addAction(new ScoreAction(5));
    Link test2 = new Link("Go to a passage", "Some passage");
    openingPassage.addLink(test);
    openingPassage.addLink(test2);
    story = new Story("TestStory2", openingPassage);
    Passage testPassage = new Passage("Next passage", "Content for this passage");
    story.addPassage(testPassage);
  }
  @Nested
  class WriteObjectToFileWithoutIssue {

    @Test
    void writesObjectToFileWithoutThrowingException() {
      assertDoesNotThrow(() -> StoryWriter.writeToFile(story));
    }

    @Test
    void writtenFileHasCorrectlyFormattedContent() {
      try {
        StoryWriter.writeToFile(story);
        //Removing new-line characters to avoid pipeline failure.
        String expected = Files.readString(Path.of("src/test/data/TestStory2.paths")).replaceAll("\\R+", "");
        String actual = Files.readString(Path.of("src/main/resources/stories/TestStory2.paths")).replaceAll("\\R+", "");

        assertEquals(expected, actual);
      } catch (IOException e) {
        fail("Test should not have thrown exception.");
      }
    }
  }
}
