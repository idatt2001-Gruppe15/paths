package fileio;

import java.io.FileNotFoundException;
import java.io.IOException;
import no.ntnu.idatt2001.paths.actions.GoldAction;
import no.ntnu.idatt2001.paths.actions.HealthAction;
import no.ntnu.idatt2001.paths.actions.InventoryAction;
import no.ntnu.idatt2001.paths.actions.ScoreAction;
import no.ntnu.idatt2001.paths.fileio.StoryReader;
import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.story.Link;
import no.ntnu.idatt2001.paths.story.Passage;
import no.ntnu.idatt2001.paths.story.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StoryReaderTest {
  Story story;
  @BeforeEach
  public void setUp() {
    Passage openingPassage = new Passage("Opening Passage", "Content for the passage");
    Link link1 = new Link("Some text", "Next passage");
    link1.addAction(new HealthAction(5));
    link1.addAction(new GoldAction(5));
    link1.addAction(new InventoryAction(new Item("Sword")));
    link1.addAction(new ScoreAction(5));

    Link link2 = new Link("Go to a passage", "Some passage");
    openingPassage.addLink(link1);
    openingPassage.addLink(link2);

    Passage nextPassage = new Passage("Next passage", "Content for this passage");

    story = new Story("TestStory", openingPassage);
    story.addPassage(nextPassage);
  }
  @Nested
  class ReadObjectMatchesWrittenObject {
    @Test
    void objectReadFromFileIsEqualToObjectWrittenToFile() throws IOException {
      Story storyRead = StoryReader.readFromFile("src/test/data/TestStory.paths");
      assertEquals(story, storyRead);
    }


  }
  @Nested
  class ReadFromFileHandlesIncorrectNamesAndFormatCorrectly {
    @Test
    void readFromFileThrowsExceptionWhenGivenIncorrectlyFormattedFile() {
      assertThrows(IllegalArgumentException.class, () -> StoryReader.readFromFile("src/test/data/IncorrectFormat.paths"));
    }
    @Test
    void readFileReturnsNullWhenGivenNonExistingStory() {
      assertThrows(FileNotFoundException.class, () -> StoryReader.readFromFile("Not a Story"));
    }

    @Test
    void readFileHandlesExtraEmptyLinesWithoutIssue() throws IOException {
      assertEquals(story, StoryReader.readFromFile("src/test/data/TestStory3.paths"));
    }
  }
}
