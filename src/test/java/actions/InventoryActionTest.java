package actions;

import no.ntnu.idatt2001.paths.actions.InventoryAction;
import static org.junit.jupiter.api.Assertions.*;

import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.story.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class InventoryActionTest {
  InventoryAction action;
  @BeforeEach
  public void setUp() {
    action = new InventoryAction(new Item("Sword"));
  }
  @Nested
  class MethodsAcceptValidParameters {
    @Test
    void constructorCreatesNonNullObject() {
      InventoryAction a = new InventoryAction(new Item("Sword"));
      assertNotNull(a);
    }

    @Test
    void executeMakesCorrectChangesToPlayerObject() {
      Player p = new Player("Test", 5, 5, 5);
      action.execute(p);
      assertEquals(new Item("Sword"), p.getInventory().get(0));
    }
  }
  @Nested
  class EqualsMethodLogicWorkingAsIntended {
    @Test
    void equalsWithSameObjectReferenceReturnsTrue() {
      assertEquals(action, action);
    }

    @Test
    void equalsWithEqualObjectsButDifferentReferenceReturnsTrue() {
      InventoryAction a2 = new InventoryAction(new Item("Sword"));
      assertEquals(action, a2);
    }

    @Test
    void equalsWithNullParameterReturnsFalse() {
      assertNotEquals(null, action);
    }

    @Test
    void equalsWithDifferentClassReturnsFalse() {
      Object o = new Object();
      assertNotEquals(action, o);
    }

    @Test
    void equalsWithSameClassButDifferentValuesReturnsFalse() {
      InventoryAction a2 = new InventoryAction(new Item("Shield"));
      assertNotEquals(action, a2);
    }
  }

  @Nested
  class MethodsThrowCorrectExceptionsWhenGivenInvalidParameters {
    @Test
    void constructorThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> new InventoryAction(null));
    }

    @Test
    void executeThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> action.execute(null));
    }
  }

  @Nested
  class GetMethodsReturnCorrectValues {
    @Test
    void toStringReturnsNonBlankString() {
      assertFalse(action.toString().isBlank());
    }
    @Test
    void getTypeReturnsInventory() {
      assertEquals("Inventory", action.getType());
    }
    @Test
    void getValueReturnsSword() {
      assertEquals("Sword", action.getValue());
    }
  }

  @Nested
  class HashCodeWorksAsIntended {
    @Test
    void hashCodeCreatesSameHashForEqualObjects() {
      InventoryAction a2 = new InventoryAction(new Item("Sword"));
      assertEquals(action.hashCode(), a2.hashCode());
    }
  }
}
