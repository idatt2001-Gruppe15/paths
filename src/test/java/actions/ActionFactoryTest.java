package actions;

import no.ntnu.idatt2001.paths.actions.ActionFactory;
import no.ntnu.idatt2001.paths.actions.GoldAction;
import no.ntnu.idatt2001.paths.actions.HealthAction;
import no.ntnu.idatt2001.paths.actions.InventoryAction;
import no.ntnu.idatt2001.paths.actions.ScoreAction;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ActionFactoryTest {
  @Nested
  class GetMethodAcceptsValidParameters {
    @Test
    void getMethodReturnsInventoryActionWhenGivenInventoryAsType() {
      assertTrue(ActionFactory.get("Inventory", "Sword") instanceof InventoryAction);
    }

    @Test
    void getMethodReturnsGoldActionWhenGivenGoldAsType() {
      assertTrue(ActionFactory.get("Gold", "5") instanceof GoldAction);
    }

    @Test
    void getMethodReturnsHealthActionWhenGivenHealthAsType() {
      assertTrue(ActionFactory.get("Health", "5") instanceof HealthAction);
    }

    @Test
    void getMethodReturnsScoreActionWhenGivenScoreAsType() {
      assertTrue(ActionFactory.get("Score", "5") instanceof ScoreAction);
    }
  }

  @Nested
  class GetMethodThrowsExceptionWhenGivenInvalidParameters {
    @Test
    void getMethodThrowsExceptionWhenGivenInvalidType() {
      assertThrows(IllegalArgumentException.class, () -> ActionFactory.get("Invalid Type", "5"));
    }
    @Test
    void getMethodThrowsExceptionWhenGivenInvalidValue() {
      assertThrows(IllegalArgumentException.class, () -> ActionFactory.get("Gold", "Sword"));
    }
  }
}
