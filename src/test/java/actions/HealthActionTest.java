package actions;

import no.ntnu.idatt2001.paths.actions.HealthAction;
import static org.junit.jupiter.api.Assertions.*;

import no.ntnu.idatt2001.paths.story.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class HealthActionTest {
  HealthAction action;
  @BeforeEach
  public void setUp() {
    action = new HealthAction(5);
  }

  @Nested
  class MethodsAcceptValidParameters {

    @Test
    void constructorCreatesNonNullObject() {
      HealthAction a = new HealthAction(5);
      assertNotNull(a);
    }

    @Test
    void executeMakesCorrectChangesToPlayerObjectWithPositiveValue() {
      Player p = new Player("Test", 5, 5, 5);
      action.execute(p);
      assertEquals(10, p.getHealth());
    }
  }

  @Nested
  class EqualsMethodLogicWorkingAsIntended {
    @Test
    void equalsWithSameObjectReferenceReturnsTrue() {
      assertEquals(action, action);
    }

    @Test
    void equalsWithEqualObjectsButDifferentReferenceReturnsTrue() {
      HealthAction a2 = new HealthAction(5);
      assertEquals(action, a2);
    }

    @Test
    void equalsWithNullParameterReturnsFalse() {
      assertNotEquals(null, action);
    }

    @Test
    void equalsWithDifferentClassReturnsFalse() {
      Object o = new Object();
      assertNotEquals(action, o);
    }

    @Test
    void equalsWithSameClassButDifferentValuesReturnsFalse() {
      HealthAction a2 = new HealthAction(6);
      assertNotEquals(action, a2);
    }
  }

  @Nested
  class MethodsThrowCorrectExceptionsWhenGivenInvalidParameters {
    @Test
    void constructorThrowsExceptionWhenGiven0AsParameter() {
      assertThrows(IllegalArgumentException.class, () -> new HealthAction(0));
    }

    @Test
    void executeThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> action.execute(null));
    }
  }

  @Nested
  class GetMethodsReturnCorrectValues {
    @Test
    void toStringReturnsNonBlankString() {
      assertFalse(action.toString().isBlank());
    }
    @Test
    void getTypeReturnsHealth() {
      assertEquals("Health", action.getType());
    }
    @Test
    void getValueReturns5() {
      assertEquals("5", action.getValue());
    }
  }
  @Nested
  class HashCodeWorksAsIntended {
    @Test
    void hashCodeReturnsSameValueForEqualObjects() {
      HealthAction a2 = new HealthAction(5);
      assertEquals(action.hashCode(), a2.hashCode());
    }
  }
}
