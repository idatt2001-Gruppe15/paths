package actions;

import no.ntnu.idatt2001.paths.actions.GoldAction;
import static org.junit.jupiter.api.Assertions.*;

import no.ntnu.idatt2001.paths.story.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class GoldActionTest {
  GoldAction action;
  @BeforeEach
  public void setUp() {
    action = new GoldAction(5);
  }

  @Nested
  class MethodsAcceptValidParameters {

    @Test
    void constructorCreatesNonNullObject() {
      GoldAction a = new GoldAction(5);
      assertNotNull(a);
    }

    @Test
    void executeMakesCorrectChangesToPlayerObject() {
      Player p = new Player("Test", 5, 5, 5);
      action.execute(p);
      assertEquals(10, p.getGold());
    }
  }

  @Nested
  class EqualsMethodLogicWorkingAsIntended {
    @Test
    void equalsWithSameObjectReferenceReturnsTrue() {
      assertEquals(action, action);
    }

    @Test
    void equalsWithEqualObjectsButDifferentReferenceReturnsTrue() {
      GoldAction a2 = new GoldAction(5);
      assertEquals(action, a2);
    }

    @Test
    void equalsWithNullParameterReturnsFalse() {
      assertNotEquals(null, action);
    }

    @Test
    void equalsWithDifferentClassReturnsFalse() {
      Object o = new Object();
      assertNotEquals(action, o);
    }

    @Test
    void equalsWithSameClassButDifferentValuesReturnsFalse() {
      GoldAction a2 = new GoldAction(6);
      assertNotEquals(action, a2);
    }
  }

  @Nested
  class MethodsThrowCorrectExceptionsWhenGivenInvalidParameters {
    @Test
    void constructorThrowsExceptionWhenGiven0AsParameter() {
      assertThrows(IllegalArgumentException.class, () -> new GoldAction(0));
    }

    @Test
    void executeThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> action.execute(null));
    }
  }
  @Nested
  class GetMethodsReturnCorrectValues {
    @Test
    void toStringReturnsNonBlankString() {
      assertFalse(action.toString().isBlank());
    }
    @Test
    void getTypeReturnsGold() {
      assertEquals("Gold", action.getType());
    }
    @Test
    void getValueReturns5() {
      assertEquals("5", action.getValue());
    }
  }

  @Nested
  class HashCodeWorksAsIntended {
    @Test
    void hashCodeReturnsSameValueForEqualObjects() {
      GoldAction a2 = new GoldAction(5);
      assertEquals(action.hashCode(), a2.hashCode());
    }
  }
}
