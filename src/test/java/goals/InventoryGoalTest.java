package goals;

import no.ntnu.idatt2001.paths.goals.InventoryGoal;
import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.story.Player;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class InventoryGoalTest {
  @Nested
  class ConstructorWorksAsIntended {
    @Test
    void constructorCreatesNonNullObjectWithValidParameters() {
      List<Item> items = new ArrayList<>();
      items.add(new Item("Sword"));
      InventoryGoal g = new InventoryGoal(items);
      assertNotNull(g);
    }
    @Test
    void constructorThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> new InventoryGoal(null));
    }
    @Test
    void constructorThrowsExceptionWhenGivenEmptyListAsParameter() {
      List<Item> items = new ArrayList<>();
      assertThrows(IllegalArgumentException.class, () -> new InventoryGoal(items));
    }
  }

  @Nested
  class IsFulfilledWorkingAsIntended {
    private final List<Item> items = new ArrayList<>(Arrays.asList(new Item("Sword"), new Item("Shield")));
    private final InventoryGoal g = new InventoryGoal(items);
    private final Player player = new Player("Name", 100, 100, 100);

    @Test
    void isFulfilledThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> g.isFulfilled(null));
    }
    @Test
    void isFulfilledReturnsTrueIfAllItemsHaveBeenObtained() {
      player.addToInventory(new Item("Sword"));
      player.addToInventory(new Item("Shield"));
      player.addToInventory(new Item("Potion"));
      assertTrue(g.isFulfilled(player));
    }
    @Test
    void isFulfilledReturnsFalseIfItemsHaveNotBeenObtained() {
      player.addToInventory(new Item("Sword"));
      assertFalse(g.isFulfilled(player));
    }
  }
}
