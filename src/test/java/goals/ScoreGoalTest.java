package goals;

import no.ntnu.idatt2001.paths.goals.ScoreGoal;
import no.ntnu.idatt2001.paths.story.Player;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ScoreGoalTest {
  @Nested
  class ConstructorWorksAsIntended {
    @Test
    void constructorCreatesNonNullObject() {
      ScoreGoal goal = new ScoreGoal(5);
      assertNotNull(goal);
    }
    @Test
    void constructorThrowsExceptionsWhenGivenNonPositiveParameter() {
      assertThrows(IllegalArgumentException.class, () -> new ScoreGoal(-1));
    }
  }
  @Nested
  class ClassCorrectlyChecksIfGoalIsAchieved {
    private final ScoreGoal goal = new ScoreGoal(5);
    @Test
    void isFulfilledThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> goal.isFulfilled(null));
    }
    @Test
    void isFulfilledReturnsTrueWhenGoalIsAchieved() {
      Player player = new Player("Name", 100, 100, 100);
      assertTrue(goal.isFulfilled(player));
    }
    @Test
    void isFulfilledReturnsFalseWhenGoalIsNotAchieved() {
      Player player = new Player("Name", 100, 1, 100);
      assertFalse(goal.isFulfilled(player));
    }
  }
}
