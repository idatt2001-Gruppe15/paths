package goals;

import no.ntnu.idatt2001.paths.goals.HealthGoal;
import no.ntnu.idatt2001.paths.story.Player;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class HealthGoalTest {
  @Nested
  class ConstructorWorksAsIntended {
    @Test
    void constructorCreatesNonNullObject() {
      HealthGoal goal = new HealthGoal(5);
      assertNotNull(goal);
    }
    @Test
    void constructorThrowsExceptionsWhenGivenNonPositiveParameter() {
      assertThrows(IllegalArgumentException.class, () -> new HealthGoal(-1));
    }
  }
  @Nested
  class ClassCorrectlyChecksIfGoalIsAchieved {
    private final HealthGoal goal = new HealthGoal(5);
    @Test
    void isFulfilledThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> goal.isFulfilled(null));
    }
    @Test
    void isFulfilledReturnsTrueWhenGoalIsAchieved() {
      Player player = new Player("Name", 100, 100, 100);
      assertTrue(goal.isFulfilled(player));
    }
    @Test
    void isFulfilledReturnsFalseWhenGoalIsNotAchieved() {
      Player player = new Player("Name", 1, 100, 100);
      assertFalse(goal.isFulfilled(player));
    }
  }
}
