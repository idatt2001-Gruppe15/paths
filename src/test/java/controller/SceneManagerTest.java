package controller;

import static org.junit.jupiter.api.Assertions.*;

import no.ntnu.idatt2001.paths.controller.SceneManager;
import org.junit.jupiter.api.Test;

class SceneManagerTest {
  @Test
  void constructorThrowsExceptionWhenGivenNullAsParameter() {
    assertThrows(IllegalArgumentException.class, () -> new SceneManager(null));
  }
}
