package controller;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import no.ntnu.idatt2001.paths.controller.PassageController;
import no.ntnu.idatt2001.paths.fileio.StoryReader;
import no.ntnu.idatt2001.paths.goals.Goal;
import no.ntnu.idatt2001.paths.goals.GoldGoal;
import no.ntnu.idatt2001.paths.story.Game;
import no.ntnu.idatt2001.paths.story.Player;
import no.ntnu.idatt2001.paths.story.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PassageControllerTest {
  private Game game;

  @BeforeEach
  void setUp() {
    Story story;
    try {
      story = StoryReader.readFromFile("src/test/data/TestStory.paths");
    } catch (IOException e) {
      return;
    }
    Player p = new Player.Builder("John", 50).build();
    List<Goal> goals = new ArrayList<>(List.of(new GoldGoal(5)));
    game = new Game(p, story, goals);
  }

  @Test
  void constructorThrowsExceptionWhenGivenNullAsSceneManager() {
    assertThrows(IllegalArgumentException.class, () -> new PassageController(null, game));
  }
}
