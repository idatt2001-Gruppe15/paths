package story;

import no.ntnu.idatt2001.paths.goals.Goal;
import no.ntnu.idatt2001.paths.goals.GoldGoal;
import no.ntnu.idatt2001.paths.story.Game;
import no.ntnu.idatt2001.paths.story.Link;
import no.ntnu.idatt2001.paths.story.Passage;
import no.ntnu.idatt2001.paths.story.Player;
import no.ntnu.idatt2001.paths.story.Story;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GameTest {
  private final Player player = new Player("Name", 100, 150, 200);
  private final Passage openingPassage = new Passage("Opening", "Content");
  private final Story story = new Story("Title", openingPassage);
  private final List<Goal> goals = new ArrayList<>();
  private final GoldGoal goal = new GoldGoal(5);

  @Nested
  class ConstructorTests {

    @Test
    void constructorAcceptsValidParameters() {
      goals.add(goal);
      Game game = new Game(player, story, goals);
      assertTrue(
              "Name".equals(game.getPlayer().getName()) &&
              game.getPlayer().getHealth() == 100 &&
              game.getPlayer().getScore() == 150 &&
              game.getPlayer().getGold() == 200
      );
      assertEquals(story, game.getStory());
      assertSame(goals.get(0), game.getGoals().get(0));
    }
    @Test
    void constructorThrowsExceptionWhenPlayerParameterIsNull() {
      goals.add(goal);
      assertThrows(IllegalArgumentException.class, () -> new Game(null, story, goals));
    }
    @Test
    void constructorThrowsExceptionWhenStoryParameterIsNull() {
      goals.add(goal);
      assertThrows(IllegalArgumentException.class, () -> new Game(player, null, goals));
    }
    @Test
    void constructorThrowsExceptionWhenGoalsParameterIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new Game(player, story, null));
    }
    @Test
    void constructorThrowsExceptionWhenGoalsParameterIsEmpty() {
      assertThrows(IllegalArgumentException.class, () -> new Game(player, story, goals));
    }
  }
  @Nested
  class GetMethodsReturnCorrectObjects {
    @Test
    void getPlayerReturnsCorrectPlayer() {
      goals.add(goal);
      Game game = new Game(player, story, goals);
      assertTrue(
              "Name".equals(game.getPlayer().getName()) &&
              game.getPlayer().getHealth() == 100 &&
              game.getPlayer().getScore() == 150 &&
              game.getPlayer().getGold() == 200
      );
    }
    @Test
    void getStoryReturnsCorrectStory() {
      goals.add(goal);
      Game game = new Game(player, story, goals);
      assertEquals(story, game.getStory());
    }
    @Test
    void getGoalsReturnCorrectGoals() {
      goals.add(goal);
      Game game = new Game(player, story, goals);
      assertSame(goals.get(0), game.getGoals().get(0));
    }
    @Test
    void beginReturnsOpeningPassageOfStory() {
      goals.add(goal);
      Game game = new Game(player, story, goals);
      assertEquals(story.getOpeningPassage(), game.begin());
    }
    @Test
    void goReturnsPassageReferencedByLink() {
      goals.add(goal);
      Link link = new Link("Title", "Title");
      Passage p = new Passage("Title", "Content");
      story.addPassage(p);
      Game game = new Game(player, story, goals);
      assertEquals(p, game.go(link));
    }
  }
}
