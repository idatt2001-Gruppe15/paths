package story;

import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.story.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {
  @Nested
  class ConstructorTests {
    @Test
    void constructorCreatesObjectWithCorrectAttributes() {
      Player player = new Player("Svein Kaare", 100, 100000, 123);
      assertEquals("Svein Kaare", player.getName());
      assertEquals(100, player.getHealth());
      assertEquals(100000, player.getScore());
      assertEquals(123, player.getGold());
      assertTrue(player.getInventory().isEmpty());
    }
    @Test
    void copyConstructorCorrectlyCopiesObject() {
      Player player1 = new Player("Erik", 100, 100, 100);
      player1.addToInventory(new Item("Sword"));
      Player player2 = new Player(player1);
      assertEquals("Erik", player2.getName());
      assertEquals(100, player2.getHealth());
      assertEquals(100, player2.getScore());
      assertEquals(100, player2.getGold());
      assertEquals(player2.getInventory().get(0), new Item("Sword"));
    }
    @Test
    void constructorThrowsExceptionWhenGivenNullAsNameParameter() {
      assertThrows(IllegalArgumentException.class, () -> new Player(null, 100, 0, 0));
    }
    @Test
    void constructorThrowsExceptionWhenGivenBlankNameAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> new Player("", 100, 0, 0));
    }
    @Test
    void constructorThrowsExceptionWhenGivenNegativeHealthAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> new Player("Svein Kaare", -10, 0, 0));
    }
    @Test
    void copyConstructorThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> new Player(null));
    }
  }

  @Nested
  class GetMethodsReturnCorrectValues {
    private final Player player = new Player("Svein Kaare", 100, 0, 0);
    @Test
    void getNameReturnsCorrectName() {
      assertEquals("Svein Kaare", player.getName());
    }
    @Test
    void getHealthReturnsCorrectHealth() {
      assertEquals(100, player.getHealth());
    }
    @Test
    void getScoreReturnsCorrectScore() {
      assertEquals(0, player.getScore());
    }
    @Test
    void getGoldReturnsCorrectGold() {
      assertEquals(0, player.getGold());
    }
    @Test
    void getHealthPropertyReturnsCorrectHealth() {
      assertEquals(100, player.getHealthProperty().get());
    }
    @Test
    void getScorePropertyReturnsCorrectScore() {
      assertEquals(0, player.getScoreProperty().get());
    }
    @Test
    void getGoldPropertyReturnsCorrectGold() {
      assertEquals(0, player.getGoldProperty().get());
    }
  }
  @Nested
  class MutatorMethodsCorrectlyAlterValues {
    private Player player;
    @BeforeEach
    public void setUp() {
      player = new Player("Svein Kaare", 100, 0, 0);
    }
    @Test
    void addHealthAddsCorrectAmountOfHealth() {
      player.addHealth(50);
      assertEquals(150, player.getHealth());
    }
    @Test
    void addHealthRemovesCorrectAmountOfHealth() {
      player.addHealth(-10);
      assertEquals(90, player.getHealth());
    }
    @Test
    void addHealthSetsHealthTo0IfHealthIsReducesBelow0() {
      player.addHealth(-110);
      assertEquals(0, player.getHealth());
    }
    @Test
    void addScoreAddsCorrectAmountOfScore() {
      player.addScore(50);
      assertEquals(50, player.getScore());
    }
    @Test
    void addScoreRemovesCorrectAmountOfScore() {
      player.addScore(-10);
      assertEquals(-10, player.getScore());
    }
    @Test
    void addGoldAddsCorrectAmountOfGold() {
      player.addGold(50);
      assertEquals(50, player.getGold());
    }
    @Test
    void addGoldRemovesCorrectAmountOfGold() {
      player.addGold(-50);
      assertEquals(-50, player.getGold());
    }
    @Test
    void testAddToInventory() {
      player.addToInventory(new Item("Sword"));
      player.addToInventory(new Item("Shield"));
      assertTrue(player.getInventory().contains(new Item("Sword")));
      assertTrue(player.getInventory().contains(new Item("Shield")));
    }

    @Test
    void removeFromInventoryRemovesItemCorrectly() {
      player.addToInventory(new Item("Sword"));
      player.removeFromInventory(new Item("Sword"));
      assertTrue(player.getInventory().isEmpty());
    }
  }
  @Nested
  class MutatorMethodsThrowExceptionsWhenGivenInvalidParameters {
    @Test
    void addToInventoryThrowsExceptionWhenGivenNullAsParameter() {
      Player player = new Player("Erik", 100, 100, 100);
      assertThrows(IllegalArgumentException.class, () -> player.addToInventory(null));
    }

    @Test
    void removeFromInventoryThrowsExceptionWhenGivenNullAsParameter() {
      Player player = new Player.Builder("Erik", 50).build();
      assertThrows(IllegalArgumentException.class, () -> player.removeFromInventory(null));
    }
  }

  @Nested
  class EqualsLogicWorksAsIntended {
    Player player1;
    Player player2;
    Player player3;
    @BeforeEach
    public void setUp() {
      player1 = new Player("Name", 10, 5, 14);
      player2 = new Player("Name", 10, 5, 14);
      player3 = new Player("Different Name", 10, 10, 10);
    }
    @Test
    void equalsWithSameReferenceReturnsTrue() {
      assertEquals(player1, player1);
    }

    @Test
    void equalsWithLogicallyEqualObjectsReturnsTrue() {
      assertEquals(player1, player2);
    }

    @Test
    void equalsWithLogicallyDifferentObjectsReturnsFalse() {
      assertNotEquals(player1, player3);
    }

    @Test
    void equalObjectsGenerateSameHashCode() {
      assertEquals(player1.hashCode(), player2.hashCode());
    }

    @Test
    void equalsWithNullReturnsFalse() {
      assertNotEquals(null, player1);
    }

    @Test
    void equalsWithDifferentClassesReturnsFalse() {
      Object o = new Object();
      assertNotEquals(player1, o);
    }

  }

  @Nested
  class PlayerBuilderAcceptsValidParameters {
    Player.Builder builder;
    @BeforeEach
    public void setUp() {
      builder = new Player.Builder("Name", 50);
    }
    @Test
    void builderCreatesObjectWithCorrectAttributes() {
      Player playerExpected = new Player("Name", 50, 10, 5);
      Player playerActual = builder.score(10).gold(5).build();
      assertEquals(playerExpected, playerActual);
    }

    @Test
    void builderWithOnlyRequiredParametersSetsOptionalParametersTo0() {
      Player playerExpected = new Player("Name", 50, 0, 0);
      Player playerActual = builder.build();
      assertEquals(playerExpected, playerActual);
    }
  }

  @Nested
  class PlayerBuilderThrowsExceptionWhenGivenInvalidParameters {
    Player.Builder builder;
    @BeforeEach
    public void setUp() {
      builder = new Player.Builder("Name", 50);
    }
    @Test
    void constructorThrowsExceptionWhenGivenNullAsName() {
      assertThrows(IllegalArgumentException.class, () -> new Player.Builder(null, 5));
    }

    @Test
    void constructorThrowsExceptionWhenGivenBlankName() {
      assertThrows(IllegalArgumentException.class, () -> new Player.Builder("", 5));
    }

    @Test
    void constructorThrowsExceptionWhenGiven0AsHealth() {
      assertThrows(IllegalArgumentException.class, () -> new Player.Builder("Name", 0));
    }

    @Test
    void scoreMethodThrowsExceptionWhenGiven0AsParameter() {
      assertThrows(IllegalArgumentException.class, () -> builder.score(0));
    }

    @Test
    void goldMethodThrowsExceptionWhenGiven0AsParameter() {
      assertThrows(IllegalArgumentException.class, () -> builder.gold(0));
    }
  }
}
