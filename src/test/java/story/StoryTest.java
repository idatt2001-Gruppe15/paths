package story;

import no.ntnu.idatt2001.paths.actions.HealthAction;
import no.ntnu.idatt2001.paths.story.Link;
import no.ntnu.idatt2001.paths.story.Passage;
import static org.junit.jupiter.api.Assertions.*;

import no.ntnu.idatt2001.paths.story.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class StoryTest {
  @Nested
  class GetMethodsReturnCorrectValues {
    private final Passage openingPassage = new Passage("Opening", "Content");
    private final Story s = new Story("Title", openingPassage);

    @Test
    void getTitleReturnsCorrectTitle() {
      assertEquals("Title", s.getTitle());
    }
    @Test
    void getOpeningPassageReturnsCorrectPassage() {
      assertEquals(openingPassage, s.getOpeningPassage());
    }
    @Test
    void getPassageReturnsCorrectPassage() {
      Link l = new Link("Title", "Title");
      Passage p = new Passage("Title", "Content");
      s.addPassage(p);
      assertEquals(p, s.getPassage(l));
    }

    @Test
    void getPassagesReturnsListOfAllPassages() {
      Story s = new Story("Title", new Passage("Opening", "Content"));
      Passage p1 = new Passage("Title 1", "Content");
      Passage p2 = new Passage("Title 2", "Content");
      s.addPassage(p1);
      s.addPassage(p2);
      List<Passage> list = new ArrayList<>();
      list.add(p1);
      list.add(p2);

      //This is done to ignore list order. Alternatively, one could use assertEquals, but this is unpredictable if the order changes.
      assertTrue(list.size() == s.getPassages().size() && list.containsAll(s.getPassages()) && s.getPassages().containsAll(list));
    }

    @Test
    void getPassageReturnsNullWhenPassageDoesNotExist() {
      assertNull(s.getPassage(new Link("Test", "Test")));
    }

    @Test
    void getBrokenLinksReturnsCorrectList() {
      Link workingLink = new Link("Test", "Passage");
      Link brokenLink1 = new Link("Test", "Nothing");
      brokenLink1.addAction(new HealthAction(5));
      Link brokenLink2 = new Link("Test", "No passage");
      Passage p1 = new Passage("Passage", "Content");
      Passage p2 = new Passage("Other passage", "Content");
      p1.addLink(brokenLink1);
      p2.addLink(workingLink);
      p2.addLink(brokenLink2);
      List<Link> expectedList = new ArrayList<>();
      expectedList.add(brokenLink1);
      expectedList.add(brokenLink2);
      s.addPassage(p1);
      s.addPassage(p2);

      List<Link> actualList = s.getBrokenLinks();

      //This is done to ignore list order. Alternatively, one could use assertEquals, but this is unpredictable if the order changes.
      assertTrue(expectedList.size() == actualList.size() && expectedList.containsAll(actualList) && actualList.containsAll(expectedList));
    }

    @Test
    void getBrokenLinksReturnBrokenListFromOpeningPassage() {
      Passage openingPassage = new Passage("Title", "Content");
      Link brokenLink = new Link("Text", "Reference");
      openingPassage.addLink(brokenLink);

      Story story = new Story("Title", openingPassage);
      assertEquals(brokenLink, story.getBrokenLinks().get(0));
    }
  }
  @Nested
  class MethodsAcceptValidParameters {
    @Test
    void constructorCreatesNonNullObject() {
      Story s = new Story("Title", new Passage("Title", "Content"));
      assertNotNull(s);
    }
    @Test
    void copyConstructorCopiesObjectCorrectly() {
      Passage p = new Passage("Title", "Content");
      Passage p2 = new Passage("Title2", "Content2");
      Story story1 = new Story("Title", p);
      story1.addPassage(p2);
      Story story2 = new Story(story1);
      assertEquals(story1, story2);
    }

    @Test
    void addPassageAddsPassageToMapWithoutIssue() {
      Passage p = new Passage("Title", "Content");
      Story s = new Story("Title", new Passage("Opening", "Content"));
      assertDoesNotThrow(() -> s.addPassage(p));
    }
    @Test
    void removePassageReturnsFalseWhenLinkParameterRefersToNonExistingPassage() {
      Story s = new Story("Title", new Passage("Opening", "Content"));
      assertFalse(s.removePassage(new Link("Test", "Not a passage")));
    }
    @Test
    void removePassageReturnsFalseIfOtherPassagesLinkToThePassageBeingRemoved() {
      Story s = new Story("Title", new Passage("Opening passage", "Content"));
      Passage firstPassage = new Passage("First", "Content");
      firstPassage.addLink(new Link("Second", "Second"));
      Passage secondPassage = new Passage("Second", "Content");
      s.addPassage(firstPassage);
      s.addPassage(secondPassage);
      assertFalse(s.removePassage(new Link("Second", "Second")));
    }
    @Test
    void removePassageReturnsTrueIfPassageIsSuccessfullyRemoved() {
      Story s = new Story("Title", new Passage("Opening passage", "Content"));
      Passage firstPassage = new Passage("First", "Content");
      firstPassage.addLink(new Link("Text", "Different passage"));
      Passage secondPassage = new Passage("Second", "Content");
      s.addPassage(firstPassage);
      s.addPassage(secondPassage);
      assertTrue(s.removePassage(new Link("Text", "Second")));
    }
  }
  @Nested
  class MethodsThrowCorrectExceptions {
    private final Story s = new Story("Title", new Passage("Title", "Content"));

    @Test
    void constructorThrowsExceptionWhenGivenNullAsTitle() {
      Passage p = new Passage("Title", "Content");
      assertThrows(IllegalArgumentException.class, () -> new Story(null, p));
    }
    @Test
    void constructorThrowsExceptionWhenGivenNullAsOpeningPassage() {
      assertThrows(IllegalArgumentException.class, () -> new Story("Title", null));
    }
    @Test
    void constructorThrowsExceptionWhenGivenBlankTitle() {
      Passage p = new Passage("Title", "Content");
      assertThrows(IllegalArgumentException.class, () -> new Story("", p));
    }
    @Test
    void copyConstructorThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> new Story(null));
    }
    @Test
    void getPassageThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> s.getPassage(null));
    }
    @Test
    void addPassageThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> s.addPassage(null));
    }
    @Test
    void addPassageThrowsExceptionWhenGivenExistingPassageAsParameter() {
      Passage p = new Passage("Different passage", "Content");
      s.addPassage(p);
      assertThrows(IllegalArgumentException.class, () -> s.addPassage(p));
    }
    @Test
    void addPassageThrowsExceptionWhenGivenPassageWithSameTitleAsOpeningPassage() {
      Passage p = new Passage("Title", "Content");
      assertThrows(IllegalArgumentException.class, () -> s.addPassage(p));
    }
    @Test
    void removePassageThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> s.removePassage(null));
    }
  }

  @Nested
  class EqualsAndHashCodeCorrectlyImplemented {
    private Story story;
    @BeforeEach
    void setUp() {
      Passage opening = new Passage("Opening Passage", "Content");
      Link l = new Link("Test", "Next Passage");
      opening.addLink(l);

      Passage nextPassage = new Passage("Next Passage", "Some content");
      Link l2 = new Link("The end", "No reference");
      nextPassage.addLink(l2);

      story = new Story("Story Title", opening);
      story.addPassage(nextPassage);
    }
    @Test
    void equalsReturnsTrueWhenGivenSameReference() {
      Story s = story;
      assertEquals(story, s);
    }

    @Test
    void equalsReturnsTrueWhenGivenLogicallyEqualObject() {
      Passage opening = new Passage("Opening Passage", "Content");
      Link l = new Link("Test", "Next Passage");
      opening.addLink(l);

      Passage nextPassage = new Passage("Next Passage", "Some content");
      Link l2 = new Link("The end", "No reference");
      nextPassage.addLink(l2);

      Story equalStory = new Story("Story Title", opening);
      equalStory.addPassage(nextPassage);

      assertEquals(story, equalStory);
    }

    @Test
    void equalsReturnsFalseWhenGivenNullAsParameter() {
      assertNotEquals(null, story);
    }

    @Test
    void equalsReturnsFalseWhenGivenDifferentClass() {
      Object o = new Object();
      assertNotEquals(story, o);
    }

    @ParameterizedTest
    @ValueSource(strings = {"Different Opening Passage", "Other Passage", "Different Story Title"})
    void equalsReturnsFalseWhenAttributesAreUnequal(String title) {
      String openingTitle = title.equals("Different Opening Passage") ? title : "Opening Passage";
      Passage opening = new Passage(openingTitle, "Content");
      Link l = new Link("Test", "Next Passage");
      opening.addLink(l);

      String passageTitle = title.equals("Other Passage") ? title : "Next Passage";
      Passage nextPassage = new Passage(passageTitle, "Some content");
      Link l2 = new Link("The end", "No reference");
      nextPassage.addLink(l2);

      String storyTitle = title.equals("Different Story Title") ? title : "Story Title";
      Story notEqualStory = new Story(storyTitle, opening);
      notEqualStory.addPassage(nextPassage);
      assertNotEquals(story, notEqualStory);
    }

    @Test
    void equalObjectsReturnSameHashCode() {
      Passage opening = new Passage("Opening Passage", "Content");
      Link l = new Link("Test", "Next Passage");
      opening.addLink(l);

      Passage nextPassage = new Passage("Next Passage", "Some content");
      Link l2 = new Link("The end", "No reference");
      nextPassage.addLink(l2);

      Story equalStory = new Story("Story Title", opening);
      equalStory.addPassage(nextPassage);

      assertEquals(story.hashCode(), equalStory.hashCode());
    }
  }
}
