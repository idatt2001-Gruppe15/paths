package story;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import no.ntnu.idatt2001.paths.actions.Action;
import no.ntnu.idatt2001.paths.actions.GoldAction;
import no.ntnu.idatt2001.paths.actions.HealthAction;
import no.ntnu.idatt2001.paths.actions.InventoryAction;
import no.ntnu.idatt2001.paths.actions.ScoreAction;
import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.story.Link;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


public class LinkTest {
  @Nested
  class GetMethodsReturnCorrectValues {
    Link l = new Link("Text", "Reference");

    @Test
    void getTextReturnsCorrectString() {
      assertEquals("Text", l.getText());
    }

    @Test
    void getReferenceReturnsCorrectString() {
      assertEquals("Reference", l.getReference());
    }

    @Test
    void getActionsReturnCorrectActions() {
      GoldAction testAction = new GoldAction(5);
      InventoryAction testAction2 = new InventoryAction(new Item("Sword"));
      l.addAction(testAction);
      l.addAction(testAction2);
      assertEquals(testAction, l.getActions().get(0));
      assertEquals(testAction2, l.getActions().get(1));
    }

    @Test
    void toStringReturnsCorrectString() {
      Link l = new Link("Text", "Reference");
      l.addAction(new GoldAction(5));
      l.addAction((new GoldAction(-5)));
      assertEquals("[Text](Reference){GoldAction:5,GoldAction:-5}", l.toString());
    }
  }

  @Nested
  class MethodsAcceptValidParameters {

    @Test
    void constructorCreatesObject() {
      Link l = new Link("Text", "Reference");
      assertNotNull(l);
    }

    @Test
    void copyConstructorCreatesEqualObject() {
      Link original = new Link("Text", "Reference");
      original.addAction(new HealthAction(5));
      Link copy = new Link(original);
      assertEquals(original, copy);
    }

    @Test
    void addActionWorksWithAll4Actions() {
      Link l = new Link("Text", "Reference");
      l.addAction(new GoldAction(5));
      l.addAction(new ScoreAction(3));
      l.addAction(new HealthAction(5));
      l.addAction(new InventoryAction(new Item("Sword")));
      List<Action> expectedActions = new ArrayList<>(Arrays.asList(
          new GoldAction(5),
          new ScoreAction(3),
          new HealthAction(5),
          new InventoryAction(new Item("Sword"))));
      assertTrue(
          expectedActions.size() == l.getActions().size()
          && expectedActions.containsAll(l.getActions())
          && l.getActions().containsAll(expectedActions));
    }
  }

  @Nested
  class EqualsLogicWorkingAsIntended {
    @Test
    void equalsWithTheSameObjectReferenceReturnTrue() {
      Link object1 = new Link("Text", "Reference");
      assertEquals(object1, object1);
    }

    @Test
    void equalsWithSameReferenceReturnsTrue() {
      Link object1 = new Link("Text", "Reference");
      object1.addAction(new HealthAction(5));

      Link object2 = new Link("Different text", "Reference");
      object2.addAction(new GoldAction(5));

      assertEquals(object1, object2);
    }

    @Test
    void equalsWithNullObjectReturnsFalseWithoutThrowingException() {
      Link l = new Link("Text", "Reference");
      assertNotEquals(null, l);
    }

    @Test
    void equalsWithDifferentClassesReturnsFalse() {
      Object o = new Object();
      Link l = new Link("Text", "Reference");
      assertNotEquals(l, o);
    }

    @Test
    void equalsWithSameClassButDifferentReferenceReturnsFalse() {
      Link l1 = new Link("Text", "Reference");
      Link l2 = new Link("Text", "Reference 2");
      assertNotEquals(l1, l2);
    }
  }
  @Nested
  class HashCodeWorkingAsIntended {
    @Test
    void equalObjectsReturnTheSameHashcode() {
      Link l1 = new Link("Text", "Reference");
      Link l2 = new Link("Text", "Reference");

      l1.addAction(new HealthAction(5));
      l2.addAction(new HealthAction(5));

      assertEquals(l1.hashCode(), l2.hashCode());
    }

    @Test
    void equalObjectsWithDifferentTextReturnSameHashCode() {
      Link l1 = new Link("Text", "Reference");
      Link l2 = new Link("Different text", "Reference");

      l1.addAction(new HealthAction(5));
      l2.addAction(new GoldAction(5));

      assertEquals(l1.hashCode(), l2.hashCode());
    }
  }

  @Nested
  class MethodsThrowCorrectExceptions {

    @Test
    void constructorThrowsExceptionWhenTextParameterIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new Link(null, "Reference"));
    }

    @Test
    void constructorThrowsExceptionWhenReferenceParameterIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new Link("Text", null));
    }

    @Test
    void constructorThrowsExceptionWhenTextIsBlank() {
      assertThrows(IllegalArgumentException.class, () -> new Link("", "Reference"));
    }

    @Test
    void constructorThrowsExceptionWhenReferenceIsBlank() {
      assertThrows(IllegalArgumentException.class, () -> new Link("Text", ""));
    }

    @Test
    void copyConstructorThrowsExceptionWhenGivenNullAsParameter() {
      assertThrows(IllegalArgumentException.class, () -> new Link(null));
    }

    @Test
    void addActionThrowsExceptionWhenParameterIsNull() {
      Link link = new Link("Text", "Reference");
      assertThrows(IllegalArgumentException.class, () -> link.addAction(null));
    }
  }
}
