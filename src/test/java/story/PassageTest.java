package story;

import no.ntnu.idatt2001.paths.actions.GoldAction;
import no.ntnu.idatt2001.paths.story.Link;
import no.ntnu.idatt2001.paths.story.Passage;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class PassageTest {
  @Nested
  class ConstructorsCreateCorrectObjects {
    @Test
    void constructorCreatesNonNullObject() {
      Passage p = new Passage("Title", "Content");
      assertNotNull(p);
    }

    @Test
    void copyConstructorCreatesEqualObject() {
      Passage p1 = new Passage("Title", "Content");
      Passage p2 = new Passage(p1);
      assertEquals(p1, p2);
    }
  }

  @Nested
  class AccessorMethodsReturnCorrectValues {
    Passage p = new Passage("Title", "Content");

    @Test
    void getTitleReturnsTitle() {
      assertEquals("Title", p.getTitle());
    }

    @Test
    void getContentReturnsContent() {
      assertEquals("Content", p.getContent());
    }

    @Test
    void getLinksReturnsListOfEqualObjects() {
      Link l1 = new Link("Text", "Reference");
      Link l2 = new Link("Text 2", "Reference 2");
      List<Link> list = new ArrayList<>();

      list.add(l1);
      list.add(l2);
      p.addLink(l1);
      p.addLink(l2);
      assertEquals(p.getLinks(), list);
    }

    @Test
    void hasLinksReturnsTrueWhenThePassageContainsLinks() {
      Passage p = new Passage("Title", "Content");
      p.addLink(new Link("Title", "Reference"));
      assertTrue(p.hasLinks());
    }

    @Test
    void hasLinksReturnsFalseWhenThePassageDoesNotContainLinks() {
      Passage p = new Passage("Title", "Content");
      assertFalse(p.hasLinks());
    }

    @Test
    void toStringReturnsNonBlankString() {
      Passage p = new Passage("Title", "Content");
      Link l = new Link("Title", "Reference");
      l.addAction(new GoldAction(5));
      p.addLink(l);
      assertFalse(p.toString().isBlank());
    }
  }

  @Nested
  class MutatorMethodsValidateParametersCorrectly {

    @Test
    void addLinkReturnsTrueWhenGivenValidParameter() {
      Passage p = new Passage("Title", "Content");
      assertTrue(p.addLink(new Link("Title", "Reference")));
    }

    @Test
    void addLinkThrowsExceptionWhenGivenNullAsParameter() {
      Passage p = new Passage("Title", "Content");
      p.addLink(new Link("Title", "Reference"));
      assertThrows(IllegalArgumentException.class, () -> p.addLink(null));
    }
  }

  @Nested
  class EqualsMethodLogicWorksAsIntended {

    @Test
    void equalsWithTheSameObjectReferenceReturnsTrue() {
      Passage p = new Passage("Title", "Content");
      assertEquals(p, p);
    }

    @Test
    void equalsWithDifferentObjectsButSameAttributesReturnsTrue() {
      Passage p1 = new Passage("Title", "Content");
      Passage p2 = new Passage("Title", "Content");
      p1.addLink(new Link("Title", "Reference"));
      p2.addLink(new Link("Title", "Reference"));
      assertEquals(p1,p2);
    }

    @Test
    void equalsWithNullReturnsFalseWithoutThrowingException() {
      Passage p = new Passage("Title", "Content");
      assertNotEquals(null, p);
    }

    @Test
    void equalsWithDifferentClassesReturnsFalse() {
      Passage p = new Passage("Title", "Content");
      Object o = new Object();
      assertNotEquals(p,o);
    }

    @Test
    void equalsWithSameClassButDifferentTitleReturnsFalse() {
      Passage p1 = new Passage("Title", "Content");
      Passage p2 = new Passage("Title 2", "Content");
      assertNotEquals(p1, p2);
    }

    @Test
    void equalsWithSameClassButDifferentContentReturnsFalse() {
      Passage p1 = new Passage("Title", "Content");
      Passage p2 = new Passage("Title", "Different content");
      assertNotEquals(p1, p2);
    }

    @Test
    void equalsWithSameClassButDifferentLinksReturnsFalse() {
      Passage p1 = new Passage("Title", "Content");
      Passage p2 = new Passage("Title", "Content");
      p1.addLink(new Link("Title", "Reference"));
      p2.addLink(new Link("Different title", "Different reference"));
      assertNotEquals(p1, p2);
    }
  }

  @Nested
  class HashCodeLogicWorksAsIntended {
    @Test
    void hashCodeWithEqualObjectsReturnsEqualHash() {
      Passage p1 = new Passage("Title", "Content");
      Passage p2 = new Passage("Title", "Content");
      Link l1 = new Link("Title", "Reference");
      Link l2 = new Link("Title", "Reference");
      l1.addAction(new GoldAction(5));
      l2.addAction(new GoldAction(5));
      p1.addLink(l1);
      p2.addLink(l2);
      assertEquals(p1.hashCode(), p2.hashCode());
    }
  }

  @Nested
  class MethodsThrowCorrectExceptions {
    @Test
    void constructorThrowsExceptionWhenTitleIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new Passage(null, "Content"));
    }

    @Test
    void constructorThrowsExceptionWhenContentIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new Passage("Title", null));
    }

    @Test
    void constructorThrowsExceptionWhenTitleIsBlank() {
      assertThrows(IllegalArgumentException.class, () -> new Passage("", "Content"));
    }

    @Test
    void constructorThrowsExceptionWhenContentIsBlank() {
      assertThrows(IllegalArgumentException.class, () -> new Passage("Title", ""));
    }

    @Test
    void copyConstructorThrowsExceptionWhenObjectIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new Passage(null));
    }
  }

}
