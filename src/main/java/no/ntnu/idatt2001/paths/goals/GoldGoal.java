package no.ntnu.idatt2001.paths.goals;

import no.ntnu.idatt2001.paths.story.Player;

/**
 * Represents a desired amount of gold for the player to reach.
 */
public class GoldGoal implements Goal {
  private final int minimumGold;

  /**
   * Constructor.
   *
   * @param minimumGold (int) The gold goal for the player. Must be a positive number.
   * @throws IllegalArgumentException If the parameter is 0 or less.
   */
  public GoldGoal(int minimumGold) {
    if (minimumGold <= 0) {
      throw new IllegalArgumentException("Minimum gold must be a positive number.");
    }
    this.minimumGold = minimumGold;
  }

  /**
   * Checks if the player has achieved the gold goal.
   *
   * @param player (Player) The player of the game.
   * @return {@code true} if the player has reached the goal, {@code false} if not.
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  @Override
  public boolean isFulfilled(Player player) {
    if (player == null) {
      throw new IllegalArgumentException("Player cannot be null.");
    }
    return player.getGold() >= minimumGold;
  }

  @Override
  public String toString() {
    return String.format("GoldGoal:%d", minimumGold);
  }
}


