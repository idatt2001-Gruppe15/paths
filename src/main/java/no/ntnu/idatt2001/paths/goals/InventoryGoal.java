package no.ntnu.idatt2001.paths.goals;

import java.util.List;
import java.util.StringJoiner;
import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.story.Player;

/**
 * Represents a list of items that the player has to obtain.
 */
public class InventoryGoal implements Goal {
  private final List<Item> mandatoryItems;

  /**
   * Constructor.
   *
   * @param mandatoryItems (List) A list of all the mandatory items to be found.
   *                       Cannot be null or empty.
   * @throws IllegalArgumentException If the list parameter is {@code null} or empty.
   */
  public InventoryGoal(List<Item> mandatoryItems) {
    if (mandatoryItems == null) {
      throw new IllegalArgumentException("List of items cannot be null.");
    }
    if (mandatoryItems.isEmpty()) {
      throw new IllegalArgumentException("List of items cannot be empty.");
    }
    this.mandatoryItems = mandatoryItems;
  }

  /**
   * Checks if the player has gotten all the mandatory items.
   *
   * @param player (Player) The player of the game.
   * @return {@code true} if the player has all the items, {@code false} if not.
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  @Override
  public boolean isFulfilled(Player player) {
    if (player == null) {
      throw new IllegalArgumentException("Player cannot be null.");
    }
    return mandatoryItems.stream()
            .allMatch(item -> player.getInventory().stream().anyMatch(item::equals));
  }

  @Override
  public String toString() {
    StringJoiner items = new StringJoiner(",");
    for (Item item : mandatoryItems) {
      items.add(item.toString());
    }
    return "InventoryGoal:{" + items + "}";
  }
}
