package no.ntnu.idatt2001.paths.goals;

import no.ntnu.idatt2001.paths.story.Player;

/**
 * Represents the desired health of the player in the game.
 */
public class HealthGoal implements Goal {
  private final int minimumHealth;

  /**
   * Constructor.
   *
   * @param minimumHealth (int) The minimum health for the player to have.
   *                      Must be a positive number.
   * @throws IllegalArgumentException If the parameter is 0 or less.
   */
  public HealthGoal(int minimumHealth) {
    if (minimumHealth <= 0) {
      throw new IllegalArgumentException("Minimum health must be a positive number.");
    }
    this.minimumHealth = minimumHealth;
  }

  /**
   * Checks if the player has achieved the health goal.
   *
   * @param player (Player) The player of the game.
   * @return {@code true} if the player has achieved the goal, {@code false} if not.
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  @Override
  public boolean isFulfilled(Player player) {
    if (player == null) {
      throw new IllegalArgumentException("Player cannot be null.");
    }
    return player.getHealth() >= minimumHealth;
  }

  @Override
  public String toString() {
    return String.format("HealthGoal:%d", minimumHealth);
  }
}