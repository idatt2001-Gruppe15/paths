package no.ntnu.idatt2001.paths.goals;

import no.ntnu.idatt2001.paths.story.Player;

/**
 * Represent a desired score for the player to reach.
 */
public class ScoreGoal implements Goal {
  private final int minimumPoints;

  /**
   * Constructor.
   *
   * @param minimumPoints (int) The score goal for the player. Must be a positive number.
   * @throws IllegalArgumentException If the parameter is 0 or less.
   */
  public ScoreGoal(int minimumPoints) {
    if (minimumPoints <= 0) {
      throw new IllegalArgumentException("minimumPoints parameter cannot be less than 0.");
    }
    this.minimumPoints = minimumPoints;
  }

  /**
   * Checks if the player has achieved the score goal.
   *
   * @param player (Player) The player of the game.
   * @return {@code true} is the player has reached the goal, {@code false} if not.
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  @Override
  public boolean isFulfilled(Player player) {
    if (player == null) {
      throw new IllegalArgumentException("Player cannot be null.");
    }
    return player.getScore() >= minimumPoints;
  }

  @Override
  public String toString() {
    return String.format("ScoreGoal:%d", minimumPoints);
  }
}



