package no.ntnu.idatt2001.paths.goals;

import no.ntnu.idatt2001.paths.story.Player;

/**
 * Represents a goal in the paths game.
 *
 * @author Erik Turmo Nordsæther
 * @version 1.0
 */

public interface Goal {
  boolean isFulfilled(Player player);
}

