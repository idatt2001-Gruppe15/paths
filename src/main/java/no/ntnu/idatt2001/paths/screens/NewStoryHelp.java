package no.ntnu.idatt2001.paths.screens;

import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.controller.SceneManager;

/**
 * Represents a screen showing the user how to create a new story.
 */
public class NewStoryHelp implements Builder<Region> {
  private final SceneManager sceneManager;
  private static final Logger LOGGER = Logger.getLogger(NewStoryHelp.class.getName());

  /**
   * Constructor. Take the scene manager for the current stage as parameter.
   *
   * @param sceneManager (SceneManager)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public NewStoryHelp(SceneManager sceneManager) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager parameter cannot be null.");
    }
    this.sceneManager = sceneManager;
  }

  @Override
  public Region build() {
    BorderPane root = new BorderPane();
    root.setTop(Components.titleBar());
    root.getStyleClass().add("main-menu");


    VBox howToPlayContent = new VBox();
    root.setCenter(howToPlayContent);
    howToPlayContent.getStyleClass().add("HTP");

    Label howToPlay = new Label("New Story");
    howToPlay.getStyleClass().add("HTP-label");
    Text guideText =
        new Text("""
                     On this page, the player will have the opportunity to write\
                      their own story. This is done by first adding a title to\
                       the story in the input field labeled "STORY TITLE".\
                        Then the player can create their own passages with links and names.\
                         The passage locked in the top left is the opening passage,\
                          and cannot be removed.
                     To create a new passage, the user can right-click on the screen and\
                      click "NEW PASSAGE". They can then give the passage a title and\
                       enter the corresponding text under "CONTENT". Finally, the user\
                        can edit the destination, text, and actions\
                         for all the links in the passage.\
                        The player can add multiple links and remove passages as needed.
                     """);
    guideText.getStyleClass().add("htpText");
    guideText.setWrappingWidth(1250);

    howToPlayContent.getChildren().addAll(howToPlay, guideText);
    howToPlayContent.setAlignment(Pos.TOP_CENTER);

    try (FileInputStream inputStream = new FileInputStream(
        "src/main/resources/images/newstory.png")) {
      ImageView guideImage = new ImageView(new Image(inputStream));


      guideImage.setFitWidth(550);
      guideImage.setFitHeight(350);
      howToPlayContent.getChildren().addAll(guideImage);
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

    HBox backButtonContainer = new HBox(4);
    root.setBottom(backButtonContainer);

    Button backButton = Components.createButton("Back");
    backButton.setOnAction(event -> sceneManager.goToMainMenu());

    Button prevPage = Components.createButton("Next");
    prevPage.setOnAction(event -> sceneManager.changeScene(new InGameHelp(sceneManager).build()));

    Button nextPage = Components.createButton("Prev");
    nextPage.setOnAction(
        event -> sceneManager.changeScene(new EditStoryHelp(sceneManager).build()));

    backButtonContainer.getChildren().addAll(backButton, nextPage, prevPage);
    backButtonContainer.setAlignment(Pos.BOTTOM_LEFT);
    backButtonContainer.setPadding(new Insets(0, 0, 10, 10));
    HBox.setHgrow(backButtonContainer, Priority.ALWAYS);
    return root;
  }
}