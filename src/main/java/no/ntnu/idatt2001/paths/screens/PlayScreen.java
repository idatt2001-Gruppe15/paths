package no.ntnu.idatt2001.paths.screens;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.components.Difficulty;
import no.ntnu.idatt2001.paths.controller.SceneManager;
import no.ntnu.idatt2001.paths.fileio.StoryReader;
import no.ntnu.idatt2001.paths.goals.Goal;
import no.ntnu.idatt2001.paths.goals.GoldGoal;
import no.ntnu.idatt2001.paths.goals.HealthGoal;
import no.ntnu.idatt2001.paths.goals.InventoryGoal;
import no.ntnu.idatt2001.paths.goals.ScoreGoal;
import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.items.ItemType;
import no.ntnu.idatt2001.paths.story.Game;
import no.ntnu.idatt2001.paths.story.Player;
import no.ntnu.idatt2001.paths.story.Story;

/**
 * A menu for starting a new game of Paths.
 */
public class PlayScreen implements Builder<Region> {
  private static final Logger LOGGER = Logger.getLogger(PlayScreen.class.getName());
  public static final String INPUT_LABEL = "input-label";
  private final SceneManager sceneManager;
  private Game game;

  /**
   * Constructor. Takes the current scene manager as parameter.
   *
   * @param sceneManager (SceneManager)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public PlayScreen(SceneManager sceneManager) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager cannot be null.");
    }
    this.sceneManager = sceneManager;
  }

  @Override
  public Region build() {
    BorderPane root = new BorderPane();
    root.getStyleClass().add("main-menu");
    root.setTop(Components.titleBar());

    VBox mainButtons = new VBox(10);

    Button newGame = Components.createButton("Custom Game");
    newGame.setOnAction(event -> newGameMenu(root));

    Button loadGame = Components.createButton("Quick Start");
    loadGame.setOnAction(event -> {
      FileChooser fileChooser = new FileChooser();
      fileChooser.setInitialDirectory(new File("src/main/resources/stories"));
      fileChooser.getExtensionFilters()
          .add(new FileChooser.ExtensionFilter("Paths game files", "*.paths"));
      File storyPath = fileChooser.showOpenDialog(sceneManager.stage());
      if (storyPath == null) {
        return;
      }
      Story story = null;
      try {
        story = StoryReader.readFromFile(storyPath.getPath());
      } catch (IllegalArgumentException e) {
        Alert error = new Alert(Alert.AlertType.ERROR);
        error.setHeaderText(null);
        error.setContentText("That file is incorrectly formatted.");
        error.showAndWait();
      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, e.getMessage(), e);
        Alert error = new Alert(Alert.AlertType.ERROR);
        error.setHeaderText(null);
        error.setContentText("An error occurred while reading from file. Please try again.");
        error.showAndWait();
      }
      if (story != null) {
        Player player = new Player.Builder("Jon Snow", 50)
                .gold(50)
                .build();
        List<Goal> goals = new ArrayList<>(Arrays.asList(new ScoreGoal(50), new GoldGoal(100)));
        Game currentGame = new Game(player, story, goals);
        startGame(currentGame);
      }
    });

    mainButtons.getChildren().addAll(newGame, loadGame);
    mainButtons.setAlignment(Pos.CENTER);

    VBox container = new VBox();
    Button back = Components.createButton("Back");
    back.setOnAction(event -> sceneManager.goToMainMenu());
    container.getChildren().add(back);
    container.setPadding(new Insets(0, 0, 5, 5));
    container.setAlignment(Pos.BOTTOM_LEFT);

    root.setCenter(mainButtons);
    root.setBottom(container);

    return root;
  }

  private void newGameMenu(BorderPane root) {
    root.getChildren().clear();
    StackPane gameCreationRoot = new StackPane();
    root.setCenter(gameCreationRoot);
    BorderPane inputElements = new BorderPane();
    AnchorPane tooltipElements = new AnchorPane();
    tooltipElements.setMouseTransparent(true);
    gameCreationRoot.getChildren().addAll(inputElements, tooltipElements);


    inputElements.setTop(Components.titleBar());

    final HBox container = new HBox(5);
    Button back = Components.createButton("Back");
    back.setOnAction(event -> sceneManager.changeScene(new PlayScreen(sceneManager).build()));
    Region spacer = new Region();
    HBox.setHgrow(spacer, Priority.ALWAYS);
    final Button start = Components.createButton("Start Game");
    final Button selectStory = Components.createButton("Select a story");
    Label currentStory = new Label("Selected story: None");
    currentStory.getStyleClass().add(INPUT_LABEL);
    FileChooser storySelector = new FileChooser();
    storySelector.setInitialDirectory(new File("src/main/resources/stories"));
    storySelector.getExtensionFilters()
        .add(new FileChooser.ExtensionFilter("Paths game files", "*.paths"));

    container.getChildren().addAll(back, spacer, currentStory, selectStory, start);
    container.setPadding(new Insets(5, 5, 5, 5));
    container.setAlignment(Pos.CENTER);
    inputElements.setBottom(container);
    Label header = new Label("Player Creation");
    header.getStyleClass().add("header");
    Label nameLabel = new Label("Name:");
    nameLabel.getStyleClass().add(INPUT_LABEL);
    TextField playerName = Components.createInputField(300);
    VBox nameContainer = new VBox();
    nameContainer.getChildren().addAll(nameLabel, playerName);
    nameContainer.setAlignment(Pos.CENTER);

    Label diffLabel = new Label("Select difficulty:");
    diffLabel.getStyleClass().add(INPUT_LABEL);
    ComboBox<Difficulty> diffSelector = new ComboBox<>();
    Arrays.stream(Difficulty.values()).forEach(diff -> diffSelector.getItems().add(diff));
    VBox diffContainer = new VBox();
    diffContainer.getChildren().addAll(diffLabel, diffSelector);
    diffContainer.setAlignment(Pos.CENTER);

    Label goalLabel = new Label("Goals (at least one):");
    goalLabel.getStyleClass().add(INPUT_LABEL);
    TextField scoreGoal = Components.createInputField(300);
    scoreGoal.setPromptText("Score goal");
    TextField goldGoal = Components.createInputField(300);
    goldGoal.setPromptText("Gold goal");
    TextField healthGoal = Components.createInputField(300);
    healthGoal.setPromptText("Health goal");

    VBox itemContainer = new VBox();
    itemContainer.setAlignment(Pos.CENTER);
    Label itemLabel = new Label("Item Goal:");
    itemLabel.getStyleClass().add("goal-label");
    ScrollPane scroller = new ScrollPane();
    ListView<CheckBox> itemSelection = new ListView<>();
    itemSelection.setMaxWidth(150);
    scroller.setContent(itemSelection);
    ObservableList<CheckBox> items = FXCollections.observableArrayList();

    Arrays.stream(ItemType.values())
        .filter(type -> !type.equals(ItemType.UNDEFINED))
        .forEach(itemType -> items.add(new CheckBox(itemType.toString())));
    itemSelection.setItems(items);
    scroller.setMaxWidth(150);
    scroller.setMaxHeight(150);
    scroller.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    scroller.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

    TextField customItemName = new TextField();
    customItemName.setMaxWidth(100);
    Button addCustomItem = new Button("Add Item");
    addCustomItem.getStyleClass().add("custom-item-button");
    HBox customItem = new HBox();
    customItem.getChildren().addAll(customItemName, addCustomItem);
    customItem.setAlignment(Pos.CENTER);
    addCustomItem.setOnAction(event -> {
      String itemName = customItemName.getText();
      if (!itemName.isBlank()) {
        items.add(new CheckBox(itemName));
      }
    });

    itemContainer.getChildren().addAll(itemLabel, scroller, customItem);

    VBox goals = new VBox(5);
    goals.getChildren().addAll(goalLabel, scoreGoal, goldGoal, healthGoal, itemContainer);
    goals.setAlignment(Pos.CENTER);

    VBox content = new VBox(20);
    content.getChildren().addAll(header, nameContainer, diffContainer, goals);
    content.setAlignment(Pos.CENTER);
    inputElements.setCenter(content);

    Label invalidName = createErrorLabel("Please enter a name");
    Label invalidDifficulty = createErrorLabel("Please select a difficulty");
    Label invalidGoals = createErrorLabel("You need to have at least one goal.");
    Label invalidGoalInput = createErrorLabel("Invalid input. Goals must be integer values.");
    Label invalidStory = createErrorLabel("Please select a story");
    Label brokenLinks = createErrorLabel("NB! This story has broken links");
    tooltipElements.getChildren().addAll(
            invalidName,
            invalidDifficulty,
            invalidGoals,
            invalidGoalInput,
            invalidStory,
            brokenLinks);

    Story[] story = new Story[1]; //This is done to allow use in lambda expression
    selectStory.setOnAction(
        event -> chooseStoryFile(currentStory, storySelector, brokenLinks, story));

    start.setOnAction(event -> {
      boolean allInputsValid = true;
      String name = playerName.getText();
      if (name.isBlank()) {
        showErrorTooltip(invalidName, playerName);
        allInputsValid = false;
      } else {
        invalidName.setVisible(false);
      }
      Difficulty difficulty = diffSelector.getValue();
      int playerHealth = 0;
      int playerGold = 0;
      if (difficulty == null) {
        showErrorTooltip(invalidDifficulty, diffSelector);
        allInputsValid = false;
      } else {
        invalidDifficulty.setVisible(false);
        playerHealth = difficulty.getHealth();
        playerGold = difficulty.getGold();
      }
      String scoreGoalString = scoreGoal.getText();
      String goldGoalString = goldGoal.getText();
      String healthGoalString = healthGoal.getText();

      List<Item> selectedItems = new ArrayList<>();
      items.forEach(checkBox -> {
        if (checkBox.isSelected()) {
          selectedItems.add(new Item(checkBox.getText()));
        }
      });

      if (scoreGoalString.isBlank()
          && goldGoalString.isBlank()
          && healthGoalString.isBlank()
          && selectedItems.isEmpty()) {
        showErrorTooltip(invalidGoals, goldGoal);
        allInputsValid = false;
      } else {
        invalidGoals.setVisible(false);
      }
      List<Goal> goalList = new ArrayList<>();
      if (!selectedItems.isEmpty()) {
        goalList.add(new InventoryGoal(selectedItems));
      }
      try {
        if (!scoreGoalString.isBlank()) {
          goalList.add(new ScoreGoal(Integer.parseInt(scoreGoalString)));
        }
        if (!goldGoalString.isBlank()) {
          goalList.add(new GoldGoal(Integer.parseInt(goldGoalString)));
        }
        if (!healthGoalString.isBlank()) {
          goalList.add(new HealthGoal(Integer.parseInt(healthGoalString)));
        }
        invalidGoalInput.setVisible(false);
      } catch (NumberFormatException e) {
        showErrorTooltip(invalidGoalInput, goldGoal);
        allInputsValid = false;
      }


      if (story[0] == null) {
        AnchorPane.setTopAnchor(
            invalidStory,
            selectStory.localToScene(selectStory.getBoundsInLocal()).getMinY() - 25.0);
        AnchorPane.setLeftAnchor(
            invalidStory,
            selectStory.localToScene(selectStory.getBoundsInLocal()).getMinX() + 5.0);
        invalidStory.setVisible(true);
        allInputsValid = false;
      } else {
        invalidStory.setVisible(false);
      }
      if (allInputsValid) {
        Player player = new Player.Builder(name, playerHealth)
                .gold(playerGold)
                .build();
        game = new Game(player, story[0], goalList);
        startGame(game);
      }
    });
  }

  private void chooseStoryFile(Label currentStory, FileChooser storySelector, Label brokenLinks,
                         Story[] story) {
    File storyFile = storySelector.showOpenDialog(sceneManager.stage());
    if (storyFile == null) {
      return;
    }
    story[0] = null;
    try {
      story[0] = StoryReader.readFromFile(storyFile.getPath());
    } catch (IllegalArgumentException e) {
      currentStory.setText("Incorrect file format");
      brokenLinks.setVisible(false);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
      Alert error = new Alert(Alert.AlertType.ERROR);
      error.setHeaderText(null);
      error.setContentText("An error occurred while reading from file. Please try again.");
      error.showAndWait();
    }
    if (story[0] != null) {
      currentStory.setText("Selected story: " + story[0].getTitle());
      if (!story[0].getBrokenLinks().isEmpty()) {
        AnchorPane.setTopAnchor(
            brokenLinks,
            currentStory.localToScene(currentStory.getBoundsInLocal()).getMinY() - 17.0);
        AnchorPane.setLeftAnchor(
            brokenLinks,
            currentStory.localToScene(currentStory.getBoundsInLocal()).getMinX());
        brokenLinks.setVisible(true);
      } else {
        brokenLinks.setVisible(false);
      }
    }
  }

  private void startGame(Game game) {
    sceneManager.changeScene(new GameScreen(sceneManager, game).build());
  }

  private Label createErrorLabel(String text) {
    Label label = new Label(text);
    label.setVisible(false);
    label.getStyleClass().add("error-tooltip");
    return label;
  }

  private void showErrorTooltip(Node tooltip, Node input) {
    AnchorPane.setTopAnchor(tooltip, input.localToScene(input.getBoundsInLocal()).getMinY() + 5.0);
    AnchorPane.setLeftAnchor(tooltip, input.localToScene(input.getBoundsInLocal()).getMaxX() + 5.0);
    tooltip.setVisible(true);
  }
}
