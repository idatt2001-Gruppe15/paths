package no.ntnu.idatt2001.paths.screens;

import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.controller.SceneManager;

/**
 * Representing a screen showing the player how to navigate the "Play" menu.
 */
public class HowToPlayScreen implements Builder<Region> {
  private static final Logger LOGGER = Logger.getLogger(HowToPlayScreen.class.getName());

  private final SceneManager sceneManager;

  /**
   * Constructor. Take the scene manager for the stage as parameter.
   *
   * @param sceneManager (SceneManager)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public HowToPlayScreen(SceneManager sceneManager) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager parameter cannot be null.");
    }
    this.sceneManager = sceneManager;
  }

  @Override
  public Region build() {
    BorderPane root = new BorderPane();
    root.setTop(Components.titleBar());
    root.getStyleClass().add("main-menu");


    VBox howToPlayContent = new VBox();
    root.setCenter(howToPlayContent);
    howToPlayContent.getStyleClass().add("HTP");

    Label howToPlay = new Label("How To Play");
    howToPlay.getStyleClass().add("HTP-label");
    Text howToPlayText = new Text(
        """
            After the player has opened Paths, they will be able to initiate a gameplay\
             session by pressing the "PLAY" button.
             Subsequently, the player will be presented with two options: "CUSTOM GAME"\
              or "QUICK START"\s
             
            By selecting "QUICK START" the player will immediately enter the game with\
             pre-established objectives after selecting a storyline.
            Alternatively, by choosing "CUSTOM GAME," the player will have the\
             opportunity to personally select the storyline, objectives, and name""");
    howToPlayText.getStyleClass().add("htpText");
    howToPlayText.setWrappingWidth(1250);

    howToPlayContent.getChildren().addAll(howToPlay, howToPlayText);
    howToPlayContent.setAlignment(Pos.TOP_CENTER);

    try (FileInputStream inputStream = new FileInputStream("src/main/resources/images/norC.png")) {
      ImageView guideImage = new ImageView(new Image(inputStream));

      guideImage.setFitWidth(400);
      guideImage.setFitHeight(350);

      howToPlayContent.getChildren().addAll(guideImage);
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

    HBox backButtonContainer = new HBox(4);
    root.setBottom(backButtonContainer);

    Button backButton = Components.createButton("Back");
    backButton.setOnAction(
            event -> sceneManager.goToMainMenu());

    Button prevPage = Components.createButton("Next");
    prevPage.setOnAction(
            event -> sceneManager.changeScene(new CustomGameHelp(sceneManager).build()));

    backButtonContainer.getChildren().addAll(backButton, prevPage);
    backButtonContainer.setAlignment(Pos.BOTTOM_LEFT);
    backButtonContainer.setPadding(new Insets(0, 0, 10, 10));
    HBox.setHgrow(backButtonContainer, Priority.ALWAYS);
    return root;
  }
}