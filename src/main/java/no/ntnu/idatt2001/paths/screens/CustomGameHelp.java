package no.ntnu.idatt2001.paths.screens;

import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.controller.SceneManager;

/**
 * Show a screen giving the player instructions on how to navigate the "Custom Game" menu.
 */
public class CustomGameHelp implements Builder<Region> {
  private static final Logger LOGGER = Logger.getLogger(CustomGameHelp.class.getName());
  private final SceneManager sceneManager;

  /**
   * Constructor. Takes the sceneManager for the stage as parameter.
   *
   * @param sceneManager (SceneManager)
   * @throws IllegalArgumentException If the scene manager parameter is {@code null}.
   */
  public CustomGameHelp(SceneManager sceneManager) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager parameter cannot be null.");
    }
    this.sceneManager = sceneManager;
  }


  @Override
  public Region build() {
    BorderPane root = new BorderPane();
    root.setTop(Components.titleBar());
    root.getStyleClass().add("main-menu");


    VBox howToPlayContent = new VBox();
    root.setCenter(howToPlayContent);
    howToPlayContent.getStyleClass().add("HTP");

    Label customGameLabel = new Label("CUSTOM GAME");
    customGameLabel.getStyleClass().add("HTP-label");
    Text howToPlayText = new Text(
            "To initiate a custom game, "
            + "the player will be prompted to fill in the following details: "
            + "name, difficulty level, objectives, and finally, "
            + "the player must choose the desired storyline. "
            + "Once all of this information is provided, "
            + "the player will be able to press \"START GAME\" to begin their custom game.");
    howToPlayText.getStyleClass().add("htpText");
    howToPlayText.setWrappingWidth(1250);

    howToPlayContent.getChildren().addAll(customGameLabel, howToPlayText);
    howToPlayContent.setAlignment(Pos.TOP_CENTER);

    try (FileInputStream inputStream = new FileInputStream(
            "src/main/resources/images/htpscreenshot.png")) {
      ImageView guideImage = new ImageView(new Image(inputStream));


      guideImage.setFitWidth(600);
      guideImage.setFitHeight(450);
      howToPlayContent.getChildren().addAll(guideImage);
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

    HBox backButtonContainer = new HBox(4);
    root.setBottom(backButtonContainer);

    Button backButton = Components.createButton("Back");
    backButton.setOnAction(event -> sceneManager.goToMainMenu());

    Button prevPage = Components.createButton("Next");
    prevPage.setOnAction(
            event -> sceneManager.changeScene(new EditStoryHelp(sceneManager).build()));

    Button nextPage = Components.createButton("Prev");
    nextPage.setOnAction(
            event -> sceneManager.changeScene(new HowToPlayScreen(sceneManager).build()));

    backButtonContainer.getChildren().addAll(backButton, nextPage, prevPage);
    backButtonContainer.setAlignment(Pos.BOTTOM_LEFT);
    backButtonContainer.setPadding(new Insets(0, 0, 10, 10));
    HBox.setHgrow(backButtonContainer, Priority.ALWAYS);
    return root;
  }
}