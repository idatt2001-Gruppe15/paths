package no.ntnu.idatt2001.paths.screens;

import java.util.ArrayList;
import java.util.List;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.components.PassageNode;
import no.ntnu.idatt2001.paths.controller.SceneManager;
import no.ntnu.idatt2001.paths.controller.StoryEditor;
import no.ntnu.idatt2001.paths.story.Passage;
import no.ntnu.idatt2001.paths.story.Story;

/**
 * Represents a menu for editing a story object in the paths game.
 */
public class StoryEditorScreen implements Builder<Region> {
  private static final String PASSAGE = "passage";
  private final Story story;
  private final SceneManager sceneManager;
  private PassageNode openingPassage;
  private final List<PassageNode> passages;

  /**
   * Constructor.
   * <p>Takes a story parameter that can be displayed to edit an existing story.</p>
   *
   * @param sceneManager (SceneManager) The manager for the current stage.
   * @param story (Story) A story object. Can be {@code null}.
   * @throws IllegalArgumentException If the sceneManager parameter is {@code null}.
   */
  public StoryEditorScreen(SceneManager sceneManager, Story story) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager cannot be null.");
    }
    this.sceneManager = sceneManager;
    this.story = story;
    passages = new ArrayList<>();
  }

  @Override
  public Region build() {
    BorderPane root = new BorderPane();
    root.getStyleClass().add("main-menu");
    final ScrollPane editorScreen = new ScrollPane();

    HBox bottomBar = new HBox();
    HBox.setHgrow(bottomBar, Priority.ALWAYS);

    VBox topBar = new VBox();
    HBox.setHgrow(topBar, Priority.ALWAYS);
    topBar.setAlignment(Pos.CENTER);

    root.setTop(topBar);
    root.setBottom(bottomBar);
    root.setCenter(editorScreen);

    Button backButton = Components.createButton("Back");
    backButton.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> {
      if (event.getButton() == MouseButton.PRIMARY) {
        sceneManager.changeScene(new StoryScreen(sceneManager).build());
      }
    });

    Region spacer = new Region();
    HBox.setHgrow(spacer, Priority.ALWAYS);

    Button createStory = Components.createButton("Create Story");

    Label errorParsingStory = new Label("Some of your inputs are invalid, please try again.");
    errorParsingStory.getStyleClass().add("input-label");
    errorParsingStory.setVisible(false);

    bottomBar.getChildren().addAll(backButton, spacer, errorParsingStory, createStory);
    bottomBar.setAlignment(Pos.BOTTOM_LEFT);
    bottomBar.setPadding(new Insets(0, 5, 5, 5));

    Label storyTitle = new Label("Story Title:");
    storyTitle.getStyleClass().clear();
    storyTitle.getStyleClass().add("input-label");

    TextField storyTitleInput = Components.createInputField(300);
    storyTitleInput.setAlignment(Pos.CENTER);
    if (story != null) {
      storyTitleInput.setText(story.getTitle());
    }

    topBar.getChildren().addAll(storyTitle, storyTitleInput);

    /* Editor with nodes representing passages. */

    //Makes a passage node draggable.
    EventHandler<MouseEvent> draggable = event -> {
      PassageNode eventNode = (PassageNode) event.getSource();

      //Sets the anchor distance to the distance from the node to the edge of the pane,
      // plus the current location of the pointer.
      double layoutX = eventNode.getLayoutX()
                       - eventNode.localToScene(eventNode.getBoundsInLocal()).getMinX()
                       + event.getSceneX();

      double layoutY = eventNode.getLayoutY()
                       - eventNode.localToScene(eventNode.getBoundsInLocal()).getMinY()
                       + event.getSceneY();

      //Preventing the nodes from being dragged off the screen.
      layoutX = layoutX < 0 ? 0.0 : layoutX;
      AnchorPane.setLeftAnchor(eventNode, layoutX);

      layoutY = layoutY < 0 ? 0.0 : layoutY;
      AnchorPane.setTopAnchor(eventNode, layoutY);
    };

    AnchorPane editorScreenContent = new AnchorPane();
    ContextMenu contextMenu = new ContextMenu();
    MenuItem newPassage = new MenuItem("New Passage");
    contextMenu.getItems().add(newPassage);
    editorScreen.setContextMenu(contextMenu);

    contextMenu.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> {
      if (event.getButton() != MouseButton.PRIMARY) {
        return;
      }
      PassageNode newPassageNode = new PassageNode();
      passages.add(newPassageNode);
      newPassageNode.getStyleClass().add(PASSAGE);
      newPassageNode.setOnMouseDragged(draggable);

      editorScreenContent.getChildren().add(newPassageNode);
      Button deleteButton = new Button("X");
      newPassageNode.getChildren().add(0, deleteButton);
      deleteButton.setOnAction(buttonPress -> {
        passages.remove(newPassageNode);
        editorScreenContent.getChildren().remove(newPassageNode);
      });

      //Positions the node right under context window.
      AnchorPane.setTopAnchor(
          newPassageNode,
          contextMenu.getY()
          - sceneManager.getStageY()
          - topBar.getHeight());

      AnchorPane.setLeftAnchor(
          newPassageNode,
          contextMenu.getX()
          - sceneManager.getStageX());
    });

    createStory.setOnAction(event -> {
      try {
        StoryEditor.createStory(storyTitleInput.getText(), openingPassage, passages);
        sceneManager.changeScene(new StoryScreen(sceneManager).build());
      } catch (Exception e) {
        errorParsingStory.setVisible(true);
      }
    });

    if (story == null) {
      openingPassage = new PassageNode();
      openingPassage.getStyleClass().add(PASSAGE);
      editorScreenContent.getChildren().add(openingPassage);
    } else {
      openingPassage = new PassageNode(story.getOpeningPassage());
      openingPassage.getStyleClass().add(PASSAGE);
      editorScreenContent.getChildren().add(openingPassage);
      double placement = 350;
      for (Passage passage : story.getPassages()) {
        PassageNode newNode = new PassageNode(passage);
        passages.add(newNode);
        Button deleteButton = new Button("X");
        newNode.getChildren().add(0, deleteButton);
        newNode.getStyleClass().add(PASSAGE);
        newNode.setOnMouseDragged(draggable);
        AnchorPane.setLeftAnchor(newNode, placement);
        AnchorPane.setTopAnchor(newNode, 0.0);
        editorScreenContent.getChildren().add(newNode);
        deleteButton.setOnAction(event -> {
          passages.remove(newNode);
          editorScreenContent.getChildren().remove(newNode);
        });
        placement += 350;
      }
    }
    editorScreen.setContent(editorScreenContent);
    return root;
  }
}
