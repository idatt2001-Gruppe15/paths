package no.ntnu.idatt2001.paths.screens;

import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.controller.PassageController;
import no.ntnu.idatt2001.paths.story.Link;
import no.ntnu.idatt2001.paths.story.Passage;

/**
 * A builder that creates screens for the passages in a story.
 */
public class PassageScreenBuilder implements Builder<Region> {
  private final PassageController passageController;
  private final Passage passage;
  private final List<Link> brokenLinks;

  /**
   * Constructor.
   * <p>Takes the passage controller for the game,
   * as well as a passage and a list of broken links as parameters.</p>
   *
   * @param passageController (PassageController)
   * @param passage (Passage) The passage to be displayed.
   * @param brokenLinks (List&lt;Link&gt;) A list of all the broken links in the story.
   * @throws IllegalArgumentException If any of the parameters are {@code null}.
   */
  public PassageScreenBuilder(PassageController passageController,
                              Passage passage,
                              List<Link> brokenLinks) {
    if (passage == null) {
      throw new IllegalArgumentException("Passage parameter cannot be null.");
    }
    if (passageController == null) {
      throw new IllegalArgumentException("Game controller parameter cannot be null.");
    }
    if (brokenLinks == null) {
      throw new IllegalArgumentException("Broken links parameter cannot be null.");
    }
    this.passageController = passageController;
    this.passage = passage;
    this.brokenLinks = brokenLinks;
  }

  @Override
  public Region build() {
    final BorderPane root = new BorderPane();
    Label title = new Label(passage.getTitle());
    title.getStyleClass().add("header");

    Text content = new Text(passage.getContent());
    content.getStyleClass().clear();
    content.getStyleClass().add("content");
    content.setWrappingWidth(500);
    StackPane contentContainer = new StackPane();
    contentContainer.getStyleClass().add("content-box");
    contentContainer.getChildren().add(content);
    contentContainer.setMaxWidth(500);

    VBox text = new VBox(50);
    text.getChildren().addAll(title, contentContainer);
    text.setAlignment(Pos.CENTER);
    text.setTranslateY(50);
    root.setTop(text);


    AnchorPane links = new AnchorPane();
    links.setMaxHeight(200);
    links.setMinHeight(200);
    links.getStyleClass().add("links");

    VBox linkButtons = new VBox(6);
    if (passage.hasLinks()) {
      passage.getLinks().forEach(link -> {
        if (brokenLinks.contains(link)) {
          linkButtons.getChildren().add(brokenLinkButton(link));
        } else {
          Button l = Components.createButton(link.getText());
          l.setOnAction(event -> passageController.changePassageScreen(link));
          linkButtons.getChildren().add(l);
        }
      });
    } else {
      Button finishGame = Components.createButton("The End");
      finishGame.setOnAction(event -> passageController.changePassageScreen(null));
      linkButtons.getChildren().add(finishGame);
    }
    AnchorPane.setTopAnchor(linkButtons, 5.0);
    AnchorPane.setLeftAnchor(linkButtons, 5.0);
    links.getChildren().add(linkButtons);

    root.setBottom(links);

    return root;
  }

  private Button brokenLinkButton(Link link) {
    Button brokenButton = new Button(link.getText());
    brokenButton.getStyleClass().add("broken-link");
    return brokenButton;
  }
}
