package no.ntnu.idatt2001.paths.screens;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.components.PlayerStats;
import no.ntnu.idatt2001.paths.controller.PassageController;
import no.ntnu.idatt2001.paths.controller.SceneManager;
import no.ntnu.idatt2001.paths.story.Game;
import no.ntnu.idatt2001.paths.story.Player;

/**
 * A screen representing the current game being played.
 */
public class GameScreen implements Builder<Region> {
  private final SceneManager sceneManager;
  private final Game game;
  private final Player initialPlayerState;

  /**
   * Constructor.
   * <p>Takes the scene manager for the current stage, as well as a Game object as parameters.</p>
   *
   * @param sceneManager (SceneManager)
   * @param game (Game) The game currently being played.
   * @throws IllegalArgumentException If any of the parameters are {@code null}.
   */
  public GameScreen(SceneManager sceneManager, Game game) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager parameter cannot be null.");
    }
    if (game == null) {
      throw new IllegalArgumentException("Game parameter cannot be null.");
    }
    this.sceneManager = sceneManager;
    this.game = game;
    initialPlayerState = new Player(game.getPlayer());
  }

  @Override
  public Region build() {
    StackPane root = new StackPane();
    root.getStyleClass().add("game");

    Node healthBar = PlayerStats.statusBarsInterface(game.getPlayer());
    AnchorPane.setTopAnchor(healthBar, 5.0);
    AnchorPane.setLeftAnchor(healthBar, 5.0);

    Node inventory = PlayerStats.inventoryInterface(game.getPlayer());
    AnchorPane.setBottomAnchor(inventory, 200.0);
    AnchorPane.setRightAnchor(inventory, 5.0);


    Button restart = Components.createButton("Restart");
    restart.setOnAction(event -> sceneManager.changeScene(
            new GameScreen(sceneManager,
                           new Game(initialPlayerState,
                                    game.getStory(),
                                    game.getGoals()))
                    .build()));

    Button quit = Components.createButton("Quit");
    quit.setOnAction(event -> sceneManager.goToMainMenu());

    HBox buttons = new HBox(5);
    buttons.getChildren().addAll(restart, quit);

    AnchorPane.setTopAnchor(buttons, 5.0);
    AnchorPane.setRightAnchor(buttons, 5.0);

    AnchorPane staticElements = new AnchorPane();
    staticElements.setPickOnBounds(false);
    staticElements.getChildren().addAll(healthBar, inventory, buttons);

    root.getChildren().addAll(new PassageController(sceneManager, game).build(), staticElements);
    return root;
  }
}
