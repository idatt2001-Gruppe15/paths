package no.ntnu.idatt2001.paths.screens;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.controller.SceneManager;

/**
 * Represents the main menu screen in the paths game.
 */
public class MainMenuScreen implements Builder<Region> {
  private final SceneManager sceneManager;

  /**
   * Constructor. Takes the scene manager for the stage as parameter.
   *
   * @param sceneManager (SceneManager)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public MainMenuScreen(SceneManager sceneManager) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager parameter cannot be null.");
    }
    this.sceneManager = sceneManager;
  }

  @Override
  public Region build() {
    BorderPane root = new BorderPane();
    root.getStyleClass().add("main-menu");

    VBox mainButtons = new VBox(10);

    Button play = Components.createButton("Play");
    play.setOnAction(event -> sceneManager.changeScene(new PlayScreen(sceneManager).build()));

    Button storyEditor = Components.createButton("Edit Story");
    storyEditor.setOnAction(
        event -> sceneManager.changeScene(new StoryScreen(sceneManager).build()));

    mainButtons.getChildren().addAll(play, storyEditor);
    mainButtons.setAlignment(Pos.CENTER);

    final HBox optionButtons = new HBox(4);

    Button credits = Components.createButton("Credits");
    credits.setOnAction(
        event -> sceneManager.changeScene(new CreditsScreen(sceneManager).build()));

    Button howToPlay = Components.createButton("How To Play");
    howToPlay.setOnAction(
        event -> sceneManager.changeScene(new HowToPlayScreen(sceneManager).build()));

    Button quit = Components.createButton("Quit");
    quit.setOnAction(event -> sceneManager.closeWindow());

    optionButtons.getChildren().addAll(credits, howToPlay, quit);
    optionButtons.setAlignment(Pos.BASELINE_CENTER);
    optionButtons.setPadding(new Insets(0, 0, 30, 0));

    root.setTop(Components.titleBar());
    root.setCenter(mainButtons);
    root.setBottom(optionButtons);

    return root;
  }
}
