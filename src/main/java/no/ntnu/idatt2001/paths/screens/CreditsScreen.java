package no.ntnu.idatt2001.paths.screens;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.controller.SceneManager;

/**
 * Represents the credits screen. Displays the different inspirations and sources for the project.
 */
public class CreditsScreen implements Builder<Region> {
  private final SceneManager sceneManager;

  /**
   * Constructor. Takes the scene manager for the stage as parameter.
   *
   * @param sceneManager (SceneManager)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public CreditsScreen(SceneManager sceneManager) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager cannot be null.");
    }
    this.sceneManager = sceneManager;
  }

  @Override
  public Region build() {
    BorderPane root = new BorderPane();
    root.setTop(Components.titleBar());
    root.getStyleClass().add("main-menu");

    VBox creditsContent = new VBox();
    root.setCenter(creditsContent);
    creditsContent.getStyleClass().add("credContent");

    Label references = new Label("References");
    references.getStyleClass().add("HTP-label");

    Text refText = new Text("""
                                Main menu picture:\s
                                pixel art.(2018). Retrieved from https://steamcommunity.com/sharedfiles/filedetails/?id=1392690693(Rolo)
                                In Game picture:
                                Pixel Art Backgrounds - Tutorial(2020). Retrieved from https://fezescalante.myportfolio.com/pixel-art-backgrounds-tutorial-skip
                                Inventory items:
                                pixel art game icon set(2017). Retrieved from https://www.shutterstock.com/image-vector/pixel-art-game-icon-set-260nw-751599646
                                \s""");

    refText.getStyleClass().add("htpText");

    Label specialThanks  = new Label("Special Thanks");
    specialThanks.getStyleClass().add("HTP-label");

    Text specThank = new Text("Takk til studass");
    specThank.getStyleClass().add("htpText");

    creditsContent.getChildren().addAll(references, refText, specialThanks, specThank);
    creditsContent.setAlignment(Pos.TOP_CENTER);
    creditsContent.getStyleClass().add("HTP");

    VBox backButtonContainer = new VBox();
    root.setBottom(backButtonContainer);

    Button backButton = Components.createButton("Back");
    backButton.setOnAction(event -> sceneManager.goToMainMenu());

    backButtonContainer.getChildren().add(backButton);
    backButtonContainer.setAlignment(Pos.BOTTOM_LEFT);
    backButtonContainer.setPadding(new Insets(0, 0, 10, 10));
    VBox.setVgrow(backButtonContainer, Priority.ALWAYS);
    return root;
  }
}
