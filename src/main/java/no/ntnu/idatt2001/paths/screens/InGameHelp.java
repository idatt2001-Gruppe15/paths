package no.ntnu.idatt2001.paths.screens;

import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.controller.SceneManager;

/**
 * Represents a screen showing the user how to play the game.
 */
public class InGameHelp implements Builder<Region> {
  private final SceneManager sceneManager;
  private static final Logger LOGGER = Logger.getLogger(InGameHelp.class.getName());

  /**
   * Constructor. Takes the scene manager for the current stage as parameter.
   *
   * @param sceneManager (SceneManager)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public InGameHelp(SceneManager sceneManager) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager parameter cannot be null.");
    }
    this.sceneManager = sceneManager;
  }

  @Override
  public Region build() {
    BorderPane root = new BorderPane();
    root.setTop(Components.titleBar());
    root.getStyleClass().add("main-menu");


    VBox howToPlayContent = new VBox();
    root.setCenter(howToPlayContent);
    howToPlayContent.getStyleClass().add("HTP");

    Label howToPlay = new Label("In game");
    howToPlay.getStyleClass().add("HTP-label");
    Text howToPlayText = new Text(
        """
            Once the game has started the user will see the player health\
             displayed in the top left corner as a red bar that decreases\
              as the player loses health. The players gold and score\
               is displayed next to it.
            In the top right the user also can see two buttons:\
             "Restart" and "Quit"
             The restart button allows the user to restart the story from the beginning,\
              while the quit button allows the user to exit to the main menu.
            """);
    howToPlayText.getStyleClass().add("htpText");
    howToPlayText.setWrappingWidth(1250);

    howToPlayContent.getChildren().addAll(howToPlay, howToPlayText);
    howToPlayContent.setAlignment(Pos.TOP_CENTER);

    try (FileInputStream inputStream = new FileInputStream("src/main/resources/images/hpBar.png")) {
      ImageView statusBarImage = new ImageView(new Image(inputStream));


      statusBarImage.setFitWidth(1250);
      statusBarImage.setFitHeight(50);
      howToPlayContent.getChildren().addAll(statusBarImage);
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

    Text howToPlayContinued = new Text(
        """

            On the right side of the game screen the user can also see the player's\
             inventory. This inventory fills up with items as the user plays through the story.\
              By clicking these items, the user can consume them to gain score ,gold, or health,\
               depending on the item used.
            NB: By using an item that is declared as a goal, the goal will not be fulfilled.\s
            """);
    howToPlayContinued.getStyleClass().add("htpText");
    howToPlayContinued.setWrappingWidth(1250);
    howToPlayContent.getChildren().addAll(howToPlayContinued);

    try (FileInputStream inputStream = new FileInputStream(
        "src/main/resources/images/inventory.png")) {
      ImageView inventoryImage = new ImageView(new Image(inputStream));


      inventoryImage.setFitWidth(200);
      inventoryImage.setFitHeight(150);
      howToPlayContent.getChildren().addAll(inventoryImage);
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

    HBox backButtonContainer = new HBox(4);
    root.setBottom(backButtonContainer);

    Button backButton = Components.createButton("Back");
    backButton.setOnAction(event -> sceneManager.goToMainMenu());

    Button nextPage = Components.createButton("Prev");
    nextPage.setOnAction(
        event -> sceneManager.changeScene(new EditStoryHelp(sceneManager).build()));

    backButtonContainer.getChildren().addAll(backButton, nextPage);
    backButtonContainer.setAlignment(Pos.BOTTOM_LEFT);
    backButtonContainer.setPadding(new Insets(0, 0, 10, 10));
    HBox.setHgrow(backButtonContainer, Priority.ALWAYS);
    return root;
  }
}