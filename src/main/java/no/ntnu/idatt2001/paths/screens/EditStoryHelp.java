package no.ntnu.idatt2001.paths.screens;

import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.controller.SceneManager;

/**
 * Represents a screen showing the player how to navigate the "Edit Story" screen.
 */
public class EditStoryHelp implements Builder<Region> {
  private static final Logger LOGGER = Logger.getLogger(EditStoryHelp.class.getName());
  private final SceneManager sceneManager;

  /**
   * Constructor. Takes the scene manager for the stage as parameter.
   *
   * @param sceneManager (SceneManager)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public EditStoryHelp(SceneManager sceneManager) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager parameter cannot be null.");
    }
    this.sceneManager = sceneManager;
  }

  @Override
  public Region build() {
    BorderPane root = new BorderPane();
    root.setTop(Components.titleBar());
    root.getStyleClass().add("main-menu");


    VBox howToPlayContent = new VBox();
    root.setCenter(howToPlayContent);
    howToPlayContent.getStyleClass().add("HTP");

    Label editStoryLabel = new Label("EDIT STORY");
    editStoryLabel.getStyleClass().add("HTP-label");
    Text guideText = new Text(
            "In the main menu, the player will have the option to upload "
            + "and create their own storylines by pressing the \"EDIT STORY\" button. "
            + "By doing so, the player will be presented with choices"
            + " to either create a new story or edit an existing one.\n");
    guideText.getStyleClass().add("htpText");
    guideText.setWrappingWidth(1250);

    howToPlayContent.getChildren().addAll(editStoryLabel, guideText);
    howToPlayContent.setAlignment(Pos.TOP_CENTER);

    try (FileInputStream inputStream = new FileInputStream("src/main/resources/images/editS.png")) {
      ImageView guideImage = new ImageView(new Image(inputStream));


      guideImage.setFitWidth(400);
      guideImage.setFitHeight(350);
      howToPlayContent.getChildren().addAll(guideImage);
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

    HBox backButtonContainer = new HBox(4);
    root.setBottom(backButtonContainer);

    Button backButton = Components.createButton("Back");
    backButton.setOnAction(event -> sceneManager.goToMainMenu());

    Button prevPage = Components.createButton("Next");
    prevPage.setOnAction(
            event -> sceneManager.changeScene(new NewStoryHelp(sceneManager).build()));

    Button nextPage = Components.createButton("Prev");
    nextPage.setOnAction(
            event -> sceneManager.changeScene(new CustomGameHelp(sceneManager).build()));

    backButtonContainer.getChildren().addAll(backButton, nextPage, prevPage);
    backButtonContainer.setAlignment(Pos.BOTTOM_LEFT);
    backButtonContainer.setPadding(new Insets(0, 0, 10, 10));
    HBox.setHgrow(backButtonContainer, Priority.ALWAYS);
    return root;
  }
}