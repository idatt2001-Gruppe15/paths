package no.ntnu.idatt2001.paths.screens;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.controller.SceneManager;
import no.ntnu.idatt2001.paths.fileio.StoryReader;
import no.ntnu.idatt2001.paths.story.Story;

/**
 * Represents a menu allowing the user to create a new story, or edit an existing one.
 */
public class StoryScreen implements Builder<Region> {
  private static final Logger LOGGER = Logger.getLogger(StoryScreen.class.getName());
  private final SceneManager sceneManager;

  /**
   * Constructor. Takes the current scene manager as parameter.
   *
   * @param sceneManager (SceneManager)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public StoryScreen(SceneManager sceneManager) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager parameter cannot be null.");
    }
    this.sceneManager = sceneManager;
  }

  @Override
  public Region build() {
    BorderPane root = new BorderPane();

    root.setTop(Components.titleBar());
    root.getStyleClass().add("main-menu");

    VBox backButtonContainer = new VBox();
    root.setLeft(backButtonContainer);

    Button backButton = Components.createButton("Back");
    backButton.setOnAction(event -> sceneManager.goToMainMenu());

    VBox container = new VBox();
    container.getChildren().add(backButton);
    container.setAlignment(Pos.BOTTOM_LEFT);
    container.setPadding(new Insets(0, 0, 10, 10));
    VBox.setVgrow(container, Priority.ALWAYS);

    final VBox buttons = new VBox(20);
    Button newButton = Components.createButton("New");
    newButton.setMinWidth(100);
    newButton.setAlignment(Pos.CENTER);
    newButton.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> {
      if (event.getButton() == MouseButton.PRIMARY) {
        sceneManager.changeScene(new StoryEditorScreen(sceneManager, null).build());
      }
    });

    Button loadButton = Components.createButton("Load");
    loadButton.setMinWidth(100);
    loadButton.setAlignment(Pos.CENTER);
    loadButton.setOnAction(event -> {
      FileChooser fileChooser = new FileChooser();
      fileChooser.getExtensionFilters()
          .add(new FileChooser.ExtensionFilter("Paths game files", "*.paths"));
      fileChooser.setInitialDirectory(new File("src/main/resources/stories"));
      File filePath = fileChooser.showOpenDialog(sceneManager.stage());
      if (filePath == null) {
        return;
      }
      Story story = null;
      try {
        story = StoryReader.readFromFile(filePath.getPath());
      } catch (IllegalArgumentException e) {
        Alert error = new Alert(Alert.AlertType.ERROR);
        error.setHeaderText(null);
        error.setContentText("That file is incorrectly formatted, "
                             + "please try a different file. ");
        error.showAndWait();
      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, e.getMessage(), e);
        Alert error = new Alert(Alert.AlertType.ERROR);
        error.setHeaderText(null);
        error.setContentText("An error occurred while reading from file. Please try again.");
        error.showAndWait();
      }
      if (story != null) {
        sceneManager.changeScene(new StoryEditorScreen(sceneManager, story).build());
      }
    });

    buttons.getChildren().addAll(newButton, loadButton, container);
    buttons.setAlignment(Pos.CENTER);
    buttons.setPadding(new Insets(40, 0, 0, 0));
    root.setCenter(buttons);

    return root;
  }
}
