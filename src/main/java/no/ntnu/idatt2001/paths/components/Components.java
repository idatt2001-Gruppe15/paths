package no.ntnu.idatt2001.paths.components;

import java.util.Objects;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableIntegerValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;

/**
 * Different standalone components used in the "paths" application.
 */
public final class Components {
  private Components() {}

  /**
   * Creates a title bar for the "paths" game.
   *
   * @return A node object representing the title bar.
   */
  public static Node titleBar() {
    VBox titleBar = new VBox();

    Label title = new Label("Paths");
    title.getStyleClass().add("title");
    Line line = new Line(0, 0, 600, 0);
    line.setStrokeWidth(3);

    titleBar.getChildren().addAll(title, line);
    titleBar.setAlignment(Pos.CENTER);
    return titleBar;
  }

  /**
   * Creates a button with the "main-button" styling.
   *
   * @param text (String) The text of the button.
   * @return A button object.
   * @throws IllegalArgumentException If the parameter is {@code null} or blank.
   */
  public static Button createButton(String text) {
    if (text == null) {
      throw new IllegalArgumentException("Text parameter cannot be null.");
    } else if (text.isBlank()) {
      throw new IllegalArgumentException("Text parameter cannot be blank.");
    }
    Button button = new Button(text);
    button.getStyleClass().clear();
    button.getStyleClass().add("main-button");
    return button;
  }

  /**
   * Creates a statusbar linked to an observable value.
   *
   * @param currentValue (ObservableIntegerValue) An observable value.
   * @param maxValue (int) The max value. This is used to decide the max width.
   * @param styleClass (String) The style class for the statusbar.
   * @return A StackPane representing the statusbar.
   * @throws IllegalArgumentException If any of the following are the case:
   *     <ul>
   *       <li>currentValue parameter is {@code null}</li>
   *       <li>maxValue parameter is 0 or less</li>
   *       <li>styleClass parameter is {@code null} or blank</li>
   *     </ul>
   */
  public static Node statusBar(ObservableIntegerValue currentValue,
                               int maxValue,
                               String styleClass) {
    StringBuilder errorMessage = new StringBuilder();
    if (currentValue == null) {
      errorMessage.append("Current value parameter cannot be null. ");
    }
    if (maxValue <= 0) {
      errorMessage.append("Max value must be a positive integer. ");
    }
    if (styleClass == null) {
      errorMessage.append("Style class parameter cannot be null. ");
    } else if (styleClass.isBlank()) {
      errorMessage.append("Style class parameter cannot be blank. ");
    }
    if (!errorMessage.isEmpty()) {
      throw new IllegalArgumentException(errorMessage.toString());
    }

    Region backgroundBar = new Region();
    backgroundBar.setMinWidth(120);
    backgroundBar.setMaxWidth(120);
    backgroundBar.setMinHeight(20);
    backgroundBar.setMaxHeight(20);

    Region valueBar = new Region();
    valueBar.setMaxHeight(18);
    valueBar.setMinHeight(18);

    final double pixelsPerPoint = 118 / (double) Objects.requireNonNull(currentValue).get();
    valueBar.setMaxWidth(118);
    valueBar.getStyleClass().add(styleClass);
    valueBar.setTranslateX(1); //Done to get the full border of the statusbar

    currentValue.addListener(
            (observable, oldValue, newValue) ->
                    valueBar.setMaxWidth(newValue.intValue() * pixelsPerPoint));

    backgroundBar.getStyleClass().add("statusbar");
    StackPane bar = new StackPane();
    bar.getChildren().addAll(backgroundBar, valueBar);
    bar.setAlignment(Pos.CENTER_LEFT);
    return bar;
  }

  /**
   * Creates an input field with the specified width.
   *
   * @param width (double)
   * @return A TextField with the "input-field" style class.
   * @throws IllegalArgumentException If the parameter is 0 or less.
   */
  public static TextField createInputField(double width) {
    if (width <= 0) {
      throw new IllegalArgumentException("Width parameter cannot be 0 or less.");
    }
    TextField field = new TextField();
    field.getStyleClass().add("input-field");
    field.setMaxWidth(width);
    return field;
  }

  /**
   * Creates a counter for an observable value.
   *
   * @param text (String) The text preceding the counter number.
   * @param counter (SimpleIntegerProperty) The value that will be shown as a counter.
   * @return A label that reflects changes in the integer property.
   * @throws IllegalArgumentException
   *     If any of the parameters are {@code null},
   *     or if the text parameter is blank.
   */
  public static Label counter(String text, SimpleIntegerProperty counter) {
    StringBuilder errorMessage = new StringBuilder();
    if (text == null) {
      errorMessage.append("Text parameter cannot be null. ");
    } else if (text.isBlank()) {
      errorMessage.append("Text parameter cannot be blank. ");
    }
    if (counter == null) {
      errorMessage.append("Counter parameter cannot be null. ");
    }
    if (!errorMessage.isEmpty()) {
      throw new IllegalArgumentException(errorMessage.toString());
    }

    Label label = new Label(text + Objects.requireNonNull(counter).get());
    counter.addListener(
            (observable, oldValue, newValue) ->
                    label.setText(text + newValue.intValue()));
    label.getStyleClass().add("input-label");
    return label;
  }
}
