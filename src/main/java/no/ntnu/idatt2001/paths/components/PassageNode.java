package no.ntnu.idatt2001.paths.components;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import no.ntnu.idatt2001.paths.actions.Action;
import no.ntnu.idatt2001.paths.actions.ActionFactory;
import no.ntnu.idatt2001.paths.story.Link;
import no.ntnu.idatt2001.paths.story.Passage;

/**
 * A Node visually representing a passage in the paths game.
 */
public class PassageNode extends VBox {
  private final TextField title;
  private final TextField content;
  private final VBox links;

  /**
   * Default constructor.
   * Creates a node that represents an empty passage in a story in the paths game.
   */
  public PassageNode() {
    title = new TextField();
    content = new TextField();
    links = new VBox();

    Button newLinkButton = new Button("Add Link");
    newLinkButton.setOnAction(event -> addLink(null));

    StackPane container = new StackPane();
    container.getChildren().add(newLinkButton);
    container.setAlignment(Pos.CENTER);

    Label titleLabel = new Label("Title:");
    Label contentLabel = new Label("Content:");
    Label linksLabel = new Label("Links:");
    getChildren().addAll(titleLabel, title, contentLabel, content, linksLabel, links, container);

    setMaxWidth(350);
    HBox.setHgrow(this, Priority.NEVER);
    setPadding(new Insets(5, 5, 5, 5));
    addLink(null);
  }

  /**
   * Reads data from a Passage object and displays it in the node.
   *
   * @param passage (Passage)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public PassageNode(Passage passage) {
    this();
    if (passage == null) {
      throw new IllegalArgumentException("Passage parameter cannot be null.");
    }
    title.setText(passage.getTitle());
    content.setText(passage.getContent());
    passage.getLinks().forEach(this::addLink);
  }

  private void addLink(Link existingLink) {
    final VBox linkContainer = new VBox();
    final HBox link = new HBox();
    final Label textLabel = new Label("Text:");
    TextField text = new TextField();
    text.setMaxWidth(100);
    final Label goTo = new Label("Go to:");
    TextField reference = new TextField();
    reference.setMaxWidth(100);

    //Adds existing data, if present.
    if (existingLink != null) {
      text.setText(existingLink.getText());
      reference.setText(existingLink.getReference());
    }

    ToggleButton edit = new ToggleButton("Edit");
    edit.selectedProperty().addListener((observable, oldValue, selected) -> {
      if (Boolean.TRUE.equals(selected)) {
        edit.setText("Done");
      } else {
        edit.setText("Edit");
      }
    });

    link.getChildren().addAll(goTo, reference, textLabel, text, edit);
    link.setAlignment(Pos.CENTER);

    VBox actions = new VBox();
    actions.setAlignment(Pos.CENTER);
    actions.visibleProperty().bind(edit.selectedProperty());
    actions.managedProperty().bind(edit.selectedProperty());
    Button newAction = new Button("New Action");
    newAction.setOnAction(event -> {
      HBox action = addAction(null);
      actions.getChildren().add(actions.getChildren().size() - 1, action);
    });
    actions.getChildren().add(newAction);

    //Adds existing actions to node, if present
    if (existingLink != null) {
      existingLink.getActions()
              .forEach(action -> actions
                      .getChildren()
                      .add(actions.getChildren().size() - 1, addAction(action)));
    }
    linkContainer.getChildren().addAll(link, actions);
    links.getChildren().add(linkContainer);
  }

  /**
   * Returns the input from the title text field.
   *
   * @return Input as a String.
   */
  public String getTitle() {
    return title.getText();
  }

  /**
   * Returns the input from the content text field.
   *
   * @return Input as a String
   */
  public String getContent() {
    return content.getText();
  }

  /**
   * Gets a list of the links represented by the input in the text fields.
   *
   * @return A list of all the valid links in the node.
   */
  public List<Link> getLinks() {
    List<Link> validLinks = new ArrayList<>();
    links.getChildren().forEach(l -> {
      VBox linkContainer = (VBox) l;
      Link link = parseLink(linkContainer);
      if (link != null) {
        validLinks.add(link);
      }
    });
    return validLinks;
  }

  private HBox addAction(Action existingAction) {
    final HBox action = new HBox();
    ComboBox<String> actionSelector = new ComboBox<>();
    ObservableList<String> actionList = FXCollections.observableArrayList(
            Arrays.asList("Inventory", "Health", "Gold", "Score"));
    actionSelector.setItems(actionList);
    TextField value = new TextField();
    value.setMaxWidth(50);
    value.setMinWidth(50);
    action.getChildren().addAll(actionSelector, value);
    if (existingAction != null) {
      actionSelector.setValue(existingAction.getType());
      value.setText(existingAction.getValue());
    }
    return action;
  }

  private Link parseLink(VBox linkContainer) {
    HBox link = (HBox) linkContainer.getChildren().get(0);
    String reference = ((TextField) link.getChildren().get(1)).getText();
    String text = ((TextField) link.getChildren().get(3)).getText();
    if (reference.isBlank() || text.isBlank()) {
      return null;
    }
    Link parsedLink = new Link(text, reference);
    VBox actions = (VBox) linkContainer.getChildren().get(1);
    for (int index = 0; index < actions.getChildren().size() - 1; index++) {
      HBox action = (HBox) actions.getChildren().get(index);
      String actionType = (String) ((ComboBox<?>) action.getChildren().get(0)).getValue();
      String actionValue = ((TextField) action.getChildren().get(1)).getText();
      if (actionType == null || actionValue.isBlank()) {
        continue;
      }
      Action linkAction = ActionFactory.get(actionType, actionValue);
      parsedLink.addAction(linkAction);
    }

    return parsedLink;
  }
}
