package no.ntnu.idatt2001.paths.components;

import java.io.FileInputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ListChangeListener;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.story.Player;

/**
 * Contains static methods for creating player status elements for the user interface.
 */
public final class PlayerStats {
  private static final Logger LOGGER = Logger.getLogger(PlayerStats.class.getName());

  private PlayerStats() {}

  /**
   * Creates user interface elements representing the different status values of the player.
   *
   * @param player (Player)
   * @return An element displaying the player's current status.
   */
  public static Region statusBarsInterface(Player player) {
    final HBox statusBars = new HBox(10);

    Label playerName = new Label(player.getName());
    playerName.getStyleClass().add("input-label");

    Node healthBar = Components.statusBar(
        player.getHealthProperty(),
        player.getHealth(),
        "health-bar");

    Label gold = Components.counter("Gold: ", player.getGoldProperty());
    gold.setAlignment(Pos.CENTER);

    Label score = Components.counter("Score: ", player.getScoreProperty());
    score.setAlignment(Pos.CENTER);

    statusBars.getChildren().addAll(playerName, healthBar, gold, score);
    statusBars.setAlignment(Pos.CENTER);
    return statusBars;
  }

  /**
   * Creates an element displaying the player's inventory.
   *
   * @param player (Player)
   * @return A box showing the current inventory of the player.
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public static Region inventoryInterface(Player player) {
    if (player == null) {
      throw new IllegalArgumentException("Player parameter cannot be null.");
    }

    FlowPane box = new FlowPane();
    player.addInventoryListener((ListChangeListener<? super Item>) change -> {
      while (change.next()) {
        if (change.wasRemoved()) {
          change.getRemoved().forEach(
                  item -> box.getChildren()
                          .stream()
                          .filter(node -> item.getType().getIconName().equals(node.getId()))
                          .findFirst()
                          .ifPresent(node -> box.getChildren().remove(node)));
        }
        if (change.wasAdded()) {
          change.getAddedSubList()
                  .forEach(item -> box.getChildren().add(createItemIcon(item, player)));
        }
      }
    });

    List<Item> playerItems = player.getInventory();
    playerItems.forEach(item -> box.getChildren().add(createItemIcon(item, player)));

    box.setMaxWidth(202);
    box.setMinWidth(202);
    box.setMaxHeight(300);
    box.setMinHeight(300);
    box.getStyleClass().add("inventory");
    return box;
  }

  private static Node createItemIcon(Item item, Player player) {
    try (FileInputStream image = new FileInputStream(
        "src/main/resources/images/" + item.getType().getIconName())) {
      ImageView icon = new ImageView(new Image(image));
      icon.setFitWidth(100);
      icon.setFitHeight(100);
      icon.setId(item.getType().getIconName());
      icon.setPickOnBounds(true);
      icon.getStyleClass().add("inventory-item");
      icon.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> {
        if (event.getButton() == MouseButton.PRIMARY) {
          item.executeAction(player);
          player.removeFromInventory(item);
        }
      });
      return icon;
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
      return null;
    }
  }
}
