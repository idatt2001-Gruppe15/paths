package no.ntnu.idatt2001.paths.components;

/**
 * Constants representing the different difficulties the player can choose from in the paths game.
 */
public enum Difficulty {
  EASY("Easy", 100, 200),
  MEDIUM("Medium", 50, 100),
  HARD("Hard", 25, 50);
  private final String name;
  private final int health;
  private final int gold;

  Difficulty(String name, int health, int gold) {
    this.name = name;
    this.health = health;
    this.gold = gold;
  }

  public int getHealth() {
    return health;
  }

  public int getGold() {
    return gold;
  }

  @Override
  public String toString() {
    return name;
  }
}
