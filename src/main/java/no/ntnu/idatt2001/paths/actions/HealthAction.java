package no.ntnu.idatt2001.paths.actions;

import java.util.Objects;
import no.ntnu.idatt2001.paths.story.Creature;

/**
 * Represents an action performed on a player's health.
 *
 * @author Svein Kåre Sørestad
 * @version 1.1
 */
public class HealthAction implements Action {
  private final int health;

  /**
   * Constructor.
   *
   * @param health (int) Health to be added or removed. Cannot be 0.
   * @throws IllegalArgumentException If the parameter is 0.
   */
  public HealthAction(int health) {
    if (health == 0) {
      throw new IllegalArgumentException("Health parameter cannot be 0.");
    }
    this.health = health;
  }

  /**
   * Changes the health of the player.
   *
   * @param player (Player)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  @Override
  public void execute(Creature player) {
    if (player == null) {
      throw new IllegalArgumentException("Player parameter cannot be null.");
    }
    player.addHealth(health);
  }

  @Override
  public String getType() {
    return "Health";
  }

  @Override
  public String getValue() {
    return String.valueOf(health);
  }

  /**
   * Overridden equals method.
   *
   * @param o (Object)
   * @return {@code true} if the objects are equal, {@code false} if not.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HealthAction that = (HealthAction) o;
    return health == that.health;
  }

  /**
   * Overridden hashCode method.
   *
   * @return A hash for the object.
   */
  @Override
  public int hashCode() {
    return Objects.hash(health);
  }

  @Override
  public String toString() {
    return "HealthAction:" + health;
  }
}
