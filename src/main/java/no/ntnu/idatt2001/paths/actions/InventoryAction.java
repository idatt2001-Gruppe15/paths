package no.ntnu.idatt2001.paths.actions;

import java.util.Objects;
import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.story.Creature;

/**
 * Represents an inventory action to be performed on the player.
 *
 * @author Svein Kåre Sørestad
 * @version 1.1
 */
public class InventoryAction implements Action {
  private final Item item;

  /**
   * Constructor.
   *
   * @param item (String) The item to be added to the player's inventory. Cannot be {@code null}.
   * @throws IllegalArgumentException If the parameter is 0.
   */
  public InventoryAction(Item item) {
    if (item == null) {
      throw new IllegalArgumentException("Item parameter cannot be null.");
    }
    this.item = item;
  }

  /**
   * Adds an item to the player's inventory.
   *
   * @param player (Player)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  @Override
  public void execute(Creature player) {
    if (player == null) {
      throw new IllegalArgumentException("Player parameter cannot be null.");
    }
    player.addToInventory(item);
  }

  @Override
  public String getType() {
    return "Inventory";
  }

  @Override
  public String getValue() {
    return item.toString();
  }

  /**
   * Overridden equals method.
   *
   * @param o (Object)
   * @return {@code true} if the objects are equal, {@code false} if not.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InventoryAction that = (InventoryAction) o;
    return item.equals(that.item);
  }

  /**
   * Overridden hashCode method.
   *
   * @return A hash for the object.
   */
  @Override
  public int hashCode() {
    return Objects.hash(item);
  }

  @Override
  public String toString() {
    return "InventoryAction:" + item;
  }
}
