package no.ntnu.idatt2001.paths.actions;

import java.util.Objects;
import no.ntnu.idatt2001.paths.story.Creature;

/**
 * Represents a gold action to be performed on the player.
 *
 * @author Svein Kåre Sørestad
 * @version 1.1
 */
public class GoldAction implements Action {
  private final int gold;

  /**
   * Constructor.
   *
   * @param gold (int) Gold to be added or removed from player. Cannot be 0.
   * @throws IllegalArgumentException If the parameter is 0.
   */
  public GoldAction(int gold) {
    if (gold == 0) {
      throw new IllegalArgumentException("Gold parameter cannot be 0.");
    }
    this.gold = gold;
  }

  /**
   * Changes the gold of the player.
   *
   * @param player (Player)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  @Override
  public void execute(Creature player) {
    if (player == null) {
      throw new IllegalArgumentException("Player parameter cannot be null.");
    }
    player.addGold(gold);
  }

  @Override
  public String getType() {
    return "Gold";
  }

  @Override
  public String getValue() {
    return String.valueOf(gold);
  }

  /**
   * Overridden equals method.
   *
   * @param o (Object)
   * @return {@code true} if the objects are equal, {@code false} if not.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GoldAction that = (GoldAction) o;
    return gold == that.gold;
  }

  /**
   * Overridden hashCode method.
   *
   * @return A hash for the object.
   */
  @Override
  public int hashCode() {
    return Objects.hash(gold);
  }

  @Override
  public String toString() {
    return "GoldAction:" + gold;
  }
}
