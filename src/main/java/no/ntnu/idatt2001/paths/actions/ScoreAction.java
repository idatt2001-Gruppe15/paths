package no.ntnu.idatt2001.paths.actions;

import java.util.Objects;
import no.ntnu.idatt2001.paths.story.Creature;

/**
 * Represents a score action to be performed on the player.
 *
 * @author Svein Kåre Sørestad
 * @version 1.1
 */
public class ScoreAction implements Action {
  private final int points;

  /**
   * Constructor.
   *
   * @param points (int) Point value to be added or removed. Cannot be 0.
   * @throws IllegalArgumentException If the parameter is 0.
   */
  public ScoreAction(int points) {
    if (points == 0) {
      throw new IllegalArgumentException("Points parameter cannot be 0.");
    }
    this.points = points;
  }

  /**
   * Changes the score of the player.
   *
   * @param player (Player)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  @Override
  public void execute(Creature player) {
    if (player == null) {
      throw new IllegalArgumentException("Player parameter cannot be null.");
    }
    player.addScore(points);
  }

  @Override
  public String getType() {
    return "Score";
  }

  @Override
  public String getValue() {
    return String.valueOf(points);
  }

  /**
   * Overridden equals method.
   *
   * @param o (Object)
   * @return {@code true} if the objects are equal, {@code false} if not.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ScoreAction that = (ScoreAction) o;
    return points == that.points;
  }

  /**
   * Overridden hashCode method.
   *
   * @return A hash for the object.
   */
  @Override
  public int hashCode() {
    return Objects.hash(points);
  }

  @Override
  public String toString() {
    return "ScoreAction:" + points;
  }
}
