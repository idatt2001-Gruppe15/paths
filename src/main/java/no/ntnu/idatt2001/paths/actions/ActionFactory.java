package no.ntnu.idatt2001.paths.actions;

import no.ntnu.idatt2001.paths.items.Item;

/**
 * Factory for action implementations.
 */
public final class ActionFactory {

  /**
   * Private constructor to avoid instantiation.
   */
  private ActionFactory() {
  }

  /**
   * Returns an instance of the class represented by the type parameter. Valid types are:
   * <ul>
   *   <li>"Inventory"</li>
   *   <li>"Gold"</li>
   *   <li>"Health"</li>
   *   <li>"Score"</li>
   * </ul>
   *
   * @param type (String) A string representing the type of action class.
   * @param value (String) The value for the action constructor.
   * @return The action class matching the type.
   * @throws IllegalArgumentException
   *           If an invalid type is given, or if the value parameter is invalid for the type.
   */
  public static Action get(final String type, final String value) {
    if (type.equalsIgnoreCase("Inventory")) {
      return new InventoryAction(new Item(value));
    } else {
      int integerValue;
      try {
        integerValue = Integer.parseInt(value);
      } catch (Exception ignored) {
        throw new IllegalArgumentException(
                String.format("Value parameter must be an integer for the %s action type.", type));
      }
      Action action;
      switch (type.toLowerCase()) {
        case "gold" -> action = new GoldAction(integerValue);
        case "health" -> action = new HealthAction(integerValue);
        case "score" -> action = new ScoreAction(integerValue);
        default -> throw new IllegalArgumentException("Type parameter is invalid.");
      }
      return action;
    }
  }
}
