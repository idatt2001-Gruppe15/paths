package no.ntnu.idatt2001.paths.actions;

import no.ntnu.idatt2001.paths.story.Creature;

/**
 * Represents an action taken in the game.
 *
 * @author Svein Kåre Sørestad
 * @version 1.0
 */
public interface Action {
  void execute(Creature creature);

  String getType();

  String getValue();
}
