package no.ntnu.idatt2001.paths;

/**
 * Runs the Paths game.
 */
public class Paths {
  public static void main(String[] args) {
    UserInterface.main(args);
  }
}
