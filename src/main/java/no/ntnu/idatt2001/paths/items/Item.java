package no.ntnu.idatt2001.paths.items;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import no.ntnu.idatt2001.paths.actions.Action;
import no.ntnu.idatt2001.paths.story.Creature;

/**
 * Represents an item in the "paths" game.
 */
public class Item {
  private final String name;
  private final ItemType type;

  /**
   * Constructor. Takes an action and an item type as parameters.
   *
   * @param type (ItemType) The type of item this is.
   * @throws IllegalArgumentException If any of the parameters are {@code null}.
   */
  public Item(ItemType type) {
    if (type == null) {
      throw new IllegalArgumentException("Type parameter cannot be null.");
    }
    this.name = type.toString();
    this.type = type;
  }

  /**
   * Constructor that takes an item name as parameter.
   * If the name is invalid, an exception is thrown.
   *
   * @param itemName (String) The name of the item.
   * @throws IllegalArgumentException
   *     If the name does not match an existing item, or if the parameter is {@code null}.
   */
  public Item(String itemName) {
    if (itemName == null) {
      throw new IllegalArgumentException("Item name parameter cannot be null.");
    }
    Optional<ItemType> matchingItemType = Arrays.stream(ItemType.values())
            .filter(itemType -> itemType.toString().equals(itemName))
            .findFirst();
    if (matchingItemType.isPresent()) {
      this.type = matchingItemType.get();
      this.name = matchingItemType.get().toString();
    } else {
      this.type = ItemType.UNDEFINED;
      name = itemName;
    }
  }

  /**
   * Executes the action associated with this item.
   *
   * @param target (Creature) The target that the item will be used on.
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public void executeAction(Creature target) {
    if (target == null) {
      throw new IllegalArgumentException("Target parameter cannot be null.");
    }
    type.getAction().execute(target);
  }

  public ItemType getType() {
    return type;
  }

  /**
   * Undefined types have a default ScoreAction of 1.
   *
   * @return The action of the type.
   */
  public Action getAction() {
    return type.getAction();
  }

  @Override
  public String toString() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Item item = (Item) o;
    return name.equals(item.name) && type == item.type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, type);
  }
}
