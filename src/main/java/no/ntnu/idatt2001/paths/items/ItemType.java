package no.ntnu.idatt2001.paths.items;

import no.ntnu.idatt2001.paths.actions.Action;
import no.ntnu.idatt2001.paths.actions.HealthAction;
import no.ntnu.idatt2001.paths.actions.ScoreAction;

/**
 * Constants for the different items found in the game.
 */
public enum ItemType {
  SWORD("Sword", new ScoreAction(5), "sword.png"),
  SHIELD("Shield", new HealthAction(2), "shield.png"),
  POTION("Potion", new HealthAction(20), "potion.png"),
  UNDEFINED("Undefined", new ScoreAction(1), "missing_icon.png");
  private final String name;
  private final Action action;
  private final String iconName;

  ItemType(String name, Action action, String iconName) {
    this.name = name;
    this.action = action;
    this.iconName = iconName;
  }

  public Action getAction() {
    return action;
  }

  public String getIconName() {
    return iconName;
  }

  @Override
  public String toString() {
    return name;
  }
}
