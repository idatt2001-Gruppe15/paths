package no.ntnu.idatt2001.paths.fileio;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import no.ntnu.idatt2001.paths.actions.GoldAction;
import no.ntnu.idatt2001.paths.actions.HealthAction;
import no.ntnu.idatt2001.paths.actions.InventoryAction;
import no.ntnu.idatt2001.paths.actions.ScoreAction;
import no.ntnu.idatt2001.paths.items.Item;
import no.ntnu.idatt2001.paths.story.Link;
import no.ntnu.idatt2001.paths.story.Passage;
import no.ntnu.idatt2001.paths.story.Story;

/**
 * Reads a Story object from a text file.
 *
 * @author Svein Kåre
 * @version 1.0
 */
public final class StoryReader {

  private StoryReader() {
  }

  /**
   * Reads an object from a file, and constructs the object represented by the text.
   *
   * @param path (String) The path to the file.
   * @return The Story object represented by the file.
   * @throws IllegalArgumentException If the file is incorrectly formatted.
   * @throws IOException If an IO error occurs.
   * @throws java.io.FileNotFoundException If the file path is invalid.
   */
  public static Story readFromFile(String path) throws IOException {
    try (Scanner reader = new Scanner(new File(path))) {

      //Reads the story title and the blank line after it.
      String storyTitle = reader.nextLine();
      if (storyTitle.startsWith("::")) {
        throw new IllegalArgumentException("Incorrect format, title is missing.");
      }
      reader.nextLine();

      //First passage is the opening passage.
      Passage openingPassage = readPassage(reader);
      if (openingPassage == null) {
        throw new IllegalArgumentException("File is incorrectly formatted.");
      }
      Story returnStory = new Story(storyTitle, openingPassage);

      //Adds the rest of the passages until the end of the file is reached.
      while (reader.hasNext()) {

        Passage nextPassage = readPassage(reader);

        //readPassage returns null if the line being read is not a passage.
        if (nextPassage != null) {
          returnStory.addPassage(nextPassage);
        }
      }
      return returnStory;
    } catch (StringIndexOutOfBoundsException e) {
      throw new IllegalArgumentException(
          "Incorrect format. Ensure that the file is spaced properly.");
    }
  }

  /**
   * Reads a single passage from the text.
   *
   * @param reader (Scanner) The scanner being used.
   * @return The Passage object represented by the text.
   */
  private static Passage readPassage(Scanner reader) {
    String nextLine = "";

    //Reads until a passage is reached
    while (reader.hasNext() && !nextLine.startsWith("::")) {
      nextLine = reader.nextLine();
    }
    //If the reader got to the end of the file without finding a passage,
    // there are no more valid passages.
    if (!reader.hasNext()) {
      return null;
    }
    String title = nextLine.substring(2);
    String content = reader.nextLine();
    if (title.isBlank() || content.isBlank()) {
      return null;
    }
    Passage returnPassage = new Passage(title, content);

    //Adds all the links in the file to the passage. Links are stored on separate lines.
    while (reader.hasNext() && !(nextLine = reader.nextLine()).isBlank()) {

      /*
        Gets the text and reference by
        finding the substrings between brackets and parentheses respectively.
      */
      String linkText = nextLine.substring(nextLine.indexOf('[') + 1, nextLine.indexOf(']'));
      String linkReference = nextLine.substring(nextLine.indexOf('(') + 1, nextLine.indexOf(')'));
      Link l = new Link(linkText, linkReference);

      //The actions are stored between curly brackets. Is empty if there are no actions.
      String actionList = nextLine.substring(nextLine.indexOf('{') + 1, nextLine.indexOf('}'));
      if (!actionList.isEmpty()) {

        //Actions are separated by ",".
        String[] actions = actionList.split(",");

        for (String action : actions) {
          //Actions are stored as key-value pairs, separated by ":".
          String[] typeAndValue = action.split(":");
          switch (typeAndValue[0]) {
            case "GoldAction" -> l.addAction(new GoldAction(Integer.parseInt(typeAndValue[1])));
            case "HealthAction" -> l.addAction(new HealthAction(Integer.parseInt(typeAndValue[1])));
            case "ScoreAction" -> l.addAction(new ScoreAction(Integer.parseInt(typeAndValue[1])));
            case "InventoryAction" -> l.addAction(new InventoryAction(new Item(typeAndValue[1])));
            default -> throw new IllegalArgumentException("Unsupported action type.");
          }
        }
      }
      returnPassage.addLink(l);
    }
    return returnPassage;
  }
}
