package no.ntnu.idatt2001.paths.fileio;

import java.io.FileWriter;
import java.io.IOException;
import no.ntnu.idatt2001.paths.story.Passage;
import no.ntnu.idatt2001.paths.story.Story;

/**
 * Writes Story objects to files. Files have the .paths extension.
 *
 * @author Svein Kåre
 * @version 2.0
 */
public final class StoryWriter {

  /**
   * Private constructor to avoid instantiation.
   */
  private StoryWriter() {
  }

  /**
   * Writes a Story object to a file in a predefined text format.
   *
   * @param story (Story)
   * @throws IOException If an I/O error occurs.
   */
  public static void writeToFile(Story story) throws IOException {
    try (FileWriter writer = new FileWriter(
            String.format("src/main/resources/stories/%s.paths", story.getTitle()))) {

      /*
        Writes the story title on the first line, followed by an empty line.
        Opening passage is written first, then the rest of the passages.
       */
      writer.write(story.getTitle() + "\r\n\r\n");
      writer.write(story.getOpeningPassage().toString());

      //Loops through the passages of the story, and writes them to file.
      for (Passage p : story.getPassages()) {
        writer.write(p.toString());
      }
    }
  }
}
