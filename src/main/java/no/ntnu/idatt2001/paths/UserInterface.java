package no.ntnu.idatt2001.paths;

import java.io.FileInputStream;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import no.ntnu.idatt2001.paths.controller.SceneManager;
import no.ntnu.idatt2001.paths.screens.MainMenuScreen;

/**
 * Starts up the user interface for the Paths game.
 */
public class UserInterface extends Application {
  private static final Logger LOGGER = Logger.getLogger(UserInterface.class.getName());

  public static void main(String[] args) {
    launch();
  }

  /**
   * Initiates the user interface elements.
   *
   * @param main (Stage) The main stage of the application.
   */
  public void start(Stage main) {
    main.setTitle("Paths");
    main.setHeight(800);
    main.setWidth(1400);
    SceneManager sceneManager = new SceneManager(main);
    Region root = new MainMenuScreen(sceneManager).build();
    Scene scene = new Scene(root);

    try (FileInputStream font = new FileInputStream(
        "src/main/resources/fonts/ARCADECLASSIC.TTF")) { //Loads a custom font.
      Font.loadFont(font, 12);
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
    scene.getStylesheets().add(
            Objects.requireNonNull(getClass().getResource("/stylesheet.css")).toExternalForm());
    main.setScene(scene);
    main.show();
  }
}
