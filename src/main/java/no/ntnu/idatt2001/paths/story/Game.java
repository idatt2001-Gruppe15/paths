package no.ntnu.idatt2001.paths.story;

import java.util.ArrayList;
import java.util.List;
import no.ntnu.idatt2001.paths.goals.Goal;  

/**
 * Represents the game itself.
 */
public class Game {
  private final Player player;
  private final Story story;
  private final List<Goal> goals;

  /**
   * Constructor.
   *
   * @param player (Player) The player of the game. Cannot be null.
   * @param story (Story)   The story of the game. Cannot be null.
   * @param goals (List)    A list of goals for the player to reach.
   *                        Cannot be null or empty.
   */
  public Game(Player player, Story story, List<Goal> goals) {
    StringBuilder message = new StringBuilder();
    if (player == null) {
      message.append("Player parameter cannot be null. ");
    }
    if (story == null) {
      message.append("Story parameter cannot be null. ");
    }
    if (goals == null) {
      message.append("Goals parameter cannot be null. ");
    } else if (goals.isEmpty()) {
      message.append("Goals parameter cannot be empty. ");
    }
    if (!message.isEmpty()) {
      throw new IllegalArgumentException(message.toString());
    }
    this.player = new Player(player);
    this.story = new Story(story);
    this.goals = new ArrayList<>();
    this.goals.addAll(goals);
  }

  /**
   * Get-method for player object.
   *
   * @return player
   */
  public Player getPlayer() {
    return player; //Reference is returned directly to allow mutation.
  }

  /**
   * Get-method for the story object.
   *
   * @return story
   */
  public Story getStory() {
    return new Story(story);
  }

  /**
   * Get-method for the goals.
   *
   * @return A list of the goals of the game.
   */
  public List<Goal> getGoals() {
    return new ArrayList<>(goals);
  }

  /**
   * Starts the game.
   *
   * @return The first passage of the game.
   */
  public Passage begin() {
    return story.getOpeningPassage();
  }

  /**
   * Moves the player to a new passage.
   *
   * @param link (Link) The link referencing the passage to go to.
   * @return The passage if it exists, {@code null} if not.
   */
  public Passage go(Link link) {
    return story.getPassage(link);
  }
}
