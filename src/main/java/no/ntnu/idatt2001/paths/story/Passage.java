package no.ntnu.idatt2001.paths.story;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents a passage in a story.
 * <p>Contains links to other passages, as well as text for user interaction.</p>
 *
 * @author Svein Kåre Sørestad
 * @version 1.0
 */
public class Passage {
  private final String title;
  private final String content;
  private final List<Link> links;

  /**
   * Constructor.
   * <p>Takes a title and content as parameters. Checks that they are not null or blank.</p>
   *
   * @param title (String)   The title of the passage. Also works as an ID.
   * @param content (String) Text for the passage.
   */
  public Passage(String title, String content) {
    StringBuilder message = new StringBuilder();
    if (title == null) {
      message.append("Title parameter cannot be null. ");
    } else if (title.isBlank()) {
      message.append("Title parameter cannot be blank. ");
    }
    if (content == null) {
      message.append("Content parameter cannot be null. ");
    } else if (content.isBlank()) {
      message.append("Content parameter cannot be blank. ");
    }
    if (!message.isEmpty()) {
      throw new IllegalArgumentException(message.toString());
    }
    this.title = title;
    this.content = content;
    links = new ArrayList<>();
  }

  /**
   * Copy constructor. Creates a deep copy of object given as parameter.
   *
   * @param passage (Passage)
   */
  public Passage(Passage passage) {
    if (passage == null) {
      throw new IllegalArgumentException("Parameter cannot be null.");
    }
    this.title = passage.title;
    this.content = passage.content;
    this.links = passage.getLinks();
  }

  /**
   * Get-method for title.
   *
   * @return title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Get-method for content.
   *
   * @return content
   */
  public String getContent() {
    return content;
  }

  /**
   * Adds a link to the Link-list. Link cannot be null.
   *
   * @param link (Link) A link to a different passage.
   * @return {@code true} if link is added successfully, {@code false} if not.
   */
  public boolean addLink(Link link) {
    if (link == null) {
      throw new IllegalArgumentException("Link cannot be null.");
    }
    return links.add(link);
  }

  /**
   * Get-method for Link-list.
   *
   * @return A list containing all the links for the passage.
   */
  public List<Link> getLinks() {
    List<Link> newList = new ArrayList<>();
    for (Link link : links) {
      newList.add(new Link(link));
    }
    return newList;
  }

  /**
   * Checks if the Link-list has any links.
   *
   * @return {@code false} if the list is empty, {@code true} if not.
   */
  public boolean hasLinks() {
    return !links.isEmpty();
  }

  /**
   * Overridden toString method.
   *
   * @return A String representing the object in text.
   */
  @Override
  public String toString() {
    StringBuilder returnString = new StringBuilder();
    returnString.append(String.format("::%s%n%s%n", title, content));

    links.forEach(link -> returnString.append(String.format("%s%n", link.toString())));
    return returnString + String.format("%n");
  }

  /**
   * Overridden equals method.
   *
   * @param o (Object) Object being compared to.
   * @return {@code true} if the objects are the same, {@code false} if not.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Passage passage = (Passage) o;
    return title.equals(passage.title)
            && content.equals(passage.content)
            && links.equals(passage.links);
  }

  /**
   * Overridden hashCode method.
   *
   * @return (int) A hash for the object.
   */
  @Override
  public int hashCode() {
    return Objects.hash(title, content, links);
  }
}
