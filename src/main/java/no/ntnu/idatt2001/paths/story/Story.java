package no.ntnu.idatt2001.paths.story;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Represents the story of a game.
 *
 * @author Svein Kåre Sørestad
 * @version 1.0
 */
public class Story {
  private final String title;
  private final Map<Link, Passage> passages;
  private final Passage openingPassage;

  /**
   * Constructor.
   * <p>Takes a title and an opening passage for the story as parameters,
   * and check that they are not null or blank.</p>
   *
   * @param title (String)           Title of the story.
   * @param openingPassage (Passage) The passage the player starts in.
   */
  public Story(String title, Passage openingPassage) {
    StringBuilder message = new StringBuilder();
    if (title == null) {
      message.append("Title cannot be null. ");
    } else if (title.isBlank()) {
      message.append("Title cannot be blank. ");
    }
    if (openingPassage == null) {
      message.append("Opening passage cannot be null. ");
    }
    if (!message.isEmpty()) {
      throw new IllegalArgumentException(message.toString());
    }
    this.title = title;
    this.passages = new HashMap<>();
    this.openingPassage = new Passage(openingPassage);
  }

  /**
   * Copy constructor.
   *
   * @param story (Story) The story to be copied.
   */
  public Story(Story story) {
    if (story == null) {
      throw new IllegalArgumentException("Story parameter cannot be null.");
    }
    this.title = story.title;
    passages = new HashMap<>();
    this.openingPassage = new Passage(story.openingPassage);
    story.passages.values().forEach(p -> addPassage(new Passage(p)));
  }

  /**
   * Get-method for title.
   *
   * @return title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Get-method for opening passage.
   *
   * @return openingPassage
   */
  public Passage getOpeningPassage() {
    return new Passage(openingPassage);
  }

  /**
   * Adds a passage to the passages map.
   *
   * @param passage (Passage) The passage to be added. Cannot be {@code null}
   */
  public void addPassage(Passage passage) {
    if (passage == null) {
      throw new IllegalArgumentException("Passage cannot be null.");
    }
    if (passage.getTitle().equals(openingPassage.getTitle())) {
      throw new IllegalArgumentException(
              "Passage title cannot be the same as the opening passage title.");
    }
    if (passages.values().stream().anyMatch(p1 -> p1.getTitle().equals(passage.getTitle()))) {
      throw new IllegalArgumentException("Cannot add multiple passages with the same title.");
    }
    passages.put(new Link(passage.getTitle(), passage.getTitle()), passage);
  }

  /**
   * Gets the passage referred to by the link given as parameter.
   *
   * @param link (Link) The link object that leads to a passage.
   * @return The passage if it exists, {@code null} if not.
   */
  public Passage getPassage(Link link) {
    if (link == null) {
      throw new IllegalArgumentException("Link cannot be null.");
    }
    Passage p = passages.get(link);
    if (p == null) {
      return null;
    } else {
      return new Passage(p);
    }
  }

  /**
   * Gets all the passages in the story.
   *
   * @return A list of the passages in the map.
   */
  public Collection<Passage> getPassages() {
    List<Passage> newList = new ArrayList<>();
    for (Passage p : passages.values()) {
      newList.add(new Passage(p));
    }
    return newList;
  }

  /**
   * Removes a passage from the story.
   *
   * @param link (Link) The link referring to the passage.
   * @return {@code true} if the passage is successfully removed, {@code false} if not.
   */
  public boolean removePassage(Link link) {
    if (link == null) {
      throw new IllegalArgumentException("Link parameter cannot be null.");
    }
    Passage value = passages.get(link);
    if (value == null) {
      return false;
    }
    //Iterates over the passages and their respective links,
    // and checks if any of the links they hold refer to the value object.
    if (passages.values()
            .stream()
            .map(Passage::getLinks)
            .flatMap(Collection::stream)
            .anyMatch(l -> l.getReference().equals(value.getTitle()))
    ) {
      return false;
    }
    return passages.remove(link) != null;
  }

  /**
   * Finds all the links in the story that refer to non-existing passages.
   *
   * @return A list of the broken links.
   */
  public List<Link> getBrokenLinks() {
    //Finding all the titles that can be referred to.
    List<String> passageTitles = passages.values()
            .stream()
            .map(Passage::getTitle)
            .toList();

    List<Passage> allPassages = new ArrayList<>(passages.values().stream().toList());
    allPassages.add(openingPassage);

    //Filter out links that refer to existing titles, and returns a list of the broken ones.
    return allPassages
            .stream()
            .map(Passage::getLinks)
            .flatMap(Collection::stream)
            .filter(l -> !passageTitles.contains(l.getReference()))
            .map(Link::new) //Deep copying stream
            .toList();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Story story = (Story) o;
    return title.equals(story.title)
           && passages.equals(story.passages)
           && openingPassage.equals(story.openingPassage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, passages, openingPassage);
  }
}
