package no.ntnu.idatt2001.paths.story;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import no.ntnu.idatt2001.paths.items.Item;

/**
 * A class representing the player with different properties.
 *
 * @author Erik nordsæther
 * @version 1.2
 */
public class Player implements Creature {
  private final String name;
  private final SimpleIntegerProperty health;
  private final SimpleIntegerProperty score;
  private final SimpleIntegerProperty gold;
  private final ObservableList<Item> inventory;

  /**
   * Constructor.
   * <p>Takes name, health, score and gold as parameters.
   * also checks that the player name is not null or blank</p>
   *
   * @param name (String)   Is the name of the player. Cannot be null or blank.
   * @param health (int)    Is the player's health, which can't be 0 or less.
   * @param score (int)     The score given through the game.
   * @param gold (int)      The amount of gold the player has.
   */
  public Player(String name, int health, int score, int gold) {
    StringBuilder message = new StringBuilder();
    if (health <= 0) {
      message.append("Health parameter cannot be 0 or less. ");
    }
    if (name == null) {
      message.append("Name parameter cannot be null. ");
    } else if (name.isBlank()) {
      message.append("Name parameter cannot be blank. ");
    }
    if (!message.isEmpty()) {
      throw new IllegalArgumentException(message.toString());
    }
    this.name = name;
    this.health = new SimpleIntegerProperty(health);
    this.score = new SimpleIntegerProperty(score);
    this.gold = new SimpleIntegerProperty(gold);
    inventory = FXCollections.observableArrayList();
  }

  /**
   * Private constructor used by builder class.
   *
   * @param builder (Builder) The builder used to construct player objects.
   */
  private Player(Builder builder) {
    this.name = builder.name;
    this.health = new SimpleIntegerProperty(builder.health);
    this.score = new SimpleIntegerProperty(builder.score);
    this.gold = new SimpleIntegerProperty(builder.gold);
    inventory = FXCollections.observableArrayList();
  }

  /**
   * Copy constructor.
   *
   * @param player (Player) The player object to be copied.
   */
  public Player(Player player) {
    if (player == null) {
      throw new IllegalArgumentException("Player parameter cannot be null.");
    }
    this.name = player.name;
    this.health = new SimpleIntegerProperty(player.health.get());
    this.score = new SimpleIntegerProperty(player.score.get());
    this.gold = new SimpleIntegerProperty(player.gold.get());
    this.inventory = FXCollections.observableArrayList();
    this.inventory.addAll(player.getInventory());
  }

  /**
   * Get-method for name.
   *
   * @return name
   */
  @Override
  public String getName() {
    return this.name;
  }

  /**
   * Get-method for health.
   *
   * @return health
   */
  @Override
  public int getHealth() {
    return this.health.get();
  }

  /**
   * Get-method for the health property.
   *
   * @return Health property.
   */
  @Override
  public SimpleIntegerProperty getHealthProperty() {
    return this.health;
  }

  /**
   * Adds health to the player. if the new health is bellow 0 the new health wil be displayed ass 0.
   *
   * @param health (int) The number to be added to the player's health.
   *               Negative numbers are allowed.
   */
  @Override
  public void addHealth(int health) {
    int newValue = Math.max((this.health.get() + health), 0);
    this.health.set(newValue);
  }


  /**
   * Get-method for score.
   *
   * @return score
   */
  @Override
  public int getScore() {
    return this.score.get();
  }

  public SimpleIntegerProperty getScoreProperty() {
    return score;
  }

  /**
   * Adds score to the player.
   *
   * @param score (int) The value to be added to the player's score. Can be a negative number.
   */
  @Override
  public void addScore(int score) {
    this.score.set(this.score.get() + score);
  }

  /**
   * Get-method for gold.
   *
   * @return gold
   */
  @Override
  public int getGold() {
    return this.gold.get();
  }

  public SimpleIntegerProperty getGoldProperty() {
    return gold;
  }

  /**
   * Adds gold to the player.
   *
   * @param gold (int) The value to be added to the player's gold. Can be a negative number.
   */
  @Override
  public void addGold(int gold) {
    this.gold.set(this.gold.get() + gold);
  }

  /**
   * Get-method for inventory of the player.
   *
   * @return A list containing all the items in the player inventory.
   */
  public List<Item> getInventory() {
    return new ArrayList<>(inventory);
  }

  /**
   * Adds a change listener to the inventory of the player.
   *
   * @param listener (ListChangeListener)
   */
  public void addInventoryListener(ListChangeListener<? super Item> listener) {
    inventory.addListener(listener);
  }

  /**
   * Adds items to the players inventory.
   *
   * @param item (String) The item to be added to the player's inventory.
   */
  @Override
  public void addToInventory(Item item) {
    if (item == null) {
      throw new IllegalArgumentException("Item cannot be null.");
    }
    inventory.add(item);
  }

  /**
   * Removes an item from the inventory of the player.
   *
   * @param item (Item) The item to be removed.
   */
  public void removeFromInventory(Item item) {
    if (item == null) {
      throw new IllegalArgumentException("Item parameter cannot be null.");
    }
    inventory.remove(item);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Player player = (Player) o;
    return health.get() == player.health.get()
           && score.get() == player.score.get()
           && gold.get() == player.gold.get()
           && name.equals(player.name)
           && inventory.equals(player.inventory);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, health.get(), score.get(), gold.get(), inventory);
  }

  /**
   * Builder for player class.
   */
  public static class Builder {
    private final String name;
    private final int health;
    private int score = 0;
    private int gold = 0;

    /**
     * Constructor.
     *
     * @param name (String) Name of the player.
     * @param health (int)  Starting health of the player.
     * @throws IllegalArgumentException
     *           If name is {@code null} or blank, or if health is 0 or less.
     */
    public Builder(String name, int health) {
      StringBuilder errorMessage = new StringBuilder();
      if (name == null) {
        errorMessage.append("Name parameter cannot be null.");
      } else if (name.isBlank()) {
        errorMessage.append("Name parameter cannot be blank.");
      }
      if (health <= 0) {
        errorMessage.append("Health cannot be 0 or less.");
      }
      if (!errorMessage.isEmpty()) {
        throw new IllegalArgumentException(errorMessage.toString());
      }
      this.name = name;
      this.health = health;
    }

    /**
     * Sets the score of the player.
     *
     * @param score (int)
     * @return {@code this}
     * @throws IllegalArgumentException If the parameter is 0 or less.
     */
    public Builder score(int score) {
      if (score <= 0) {
        throw new IllegalArgumentException("Score parameter cannot be 0 or less.");
      }
      this.score = score;
      return this;
    }

    /**
     * Sets the gold of the player.
     *
     * @param gold (int)
     * @return {@code this}
     * @throws IllegalArgumentException If the parameter is 0 or less.
     */
    public Builder gold(int gold) {
      if (gold <= 0) {
        throw new IllegalArgumentException("Gold parameter cannot be 0 or less.");
      }
      this.gold = gold;
      return this;
    }

    /**
     * Finalizes the object, and creates an instance of the player class.
     *
     * @return The player object created by the builder.
     */
    public Player build() {
      return new Player(this);
    }
  }
}
