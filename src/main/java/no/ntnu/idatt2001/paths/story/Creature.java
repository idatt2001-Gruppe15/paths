package no.ntnu.idatt2001.paths.story;

import javafx.beans.property.SimpleIntegerProperty;
import no.ntnu.idatt2001.paths.items.Item;

/**
 * Defines methods shared by all types of creatures.
 */
public interface Creature {
  int getHealth();

  SimpleIntegerProperty getHealthProperty();

  void addHealth(int health);

  String getName();

  int getScore();

  void addScore(int score);

  int getGold();

  void addGold(int gold);

  void addToInventory(Item item);
}
