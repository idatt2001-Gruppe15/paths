package no.ntnu.idatt2001.paths.story;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import no.ntnu.idatt2001.paths.actions.Action;

/**
 * Represent a link to a different passage in a story.
 * <p>Contains actions that are taken when the link is selected.</p>
 *
 * @author Svein Kåre Sørestad
 * @version 1.1
 */
public class Link {
  private final String text;
  private final String reference;
  private final ArrayList<Action> actions;

  /**
   * Constructor.
   * <p>Takes a text and a reference as parameters, and ensures that they are not null or empty.</p>
   *
   * @param text (String)      A text describing a choice the player can make.
   * @param reference (String) The title of the passage that the link goes to.
   */
  public Link(String text, String reference) {
    StringBuilder message = new StringBuilder();
    if (text == null) {
      message.append("Text parameter cannot be null. ");
    } else if (text.isBlank()) {
      message.append("Text parameter cannot be blank. ");
    }
    if (reference == null) {
      message.append("Reference parameter cannot be null. ");
    } else if (reference.isBlank()) {
      message.append("Reference parameter cannot be blank. ");
    }
    if (!message.isEmpty()) {
      throw new IllegalArgumentException(message.toString());
    }
    this.text = text;
    this.reference = reference;
    actions = new ArrayList<>();
  }

  /**
   * Copy constructor. Creates a deep copy of object given as parameter.
   *
   * @param link (Link)
   */
  public Link(Link link) {
    if (link == null) {
      throw new IllegalArgumentException("Parameter cannot be null.");
    }
    this.text = link.text;
    this.reference = link.reference;
    this.actions = (ArrayList<Action>) link.getActions();
  }

  /**
   * Get-method for text.
   *
   * @return text
   */
  public String getText() {
    return text;
  }

  /**
   * Get-method for reference.
   *
   * @return reference
   */
  public String getReference() {
    return reference;
  }

  /**
   * Adds an action to the link.
   *
   * @param action (Action) An action that happens when this link is selected.
   */
  public void addAction(Action action) {
    if (action == null) {
      throw new IllegalArgumentException("Action cannot be null.");
    }
    actions.add(action);
  }

  /**
   * Get-method for action list.
   *
   * @return A list containing all the actions for the link.
   */
  public List<Action> getActions() {
    return new ArrayList<>(actions);
  }

  /**
   * Overridden toString method.
   *
   * @return (String) A string representing the class.
   */
  @Override
  public String toString() {
    StringBuilder returnString = new StringBuilder();
    returnString.append(String.format("[%s](%s)", text, reference));
    returnString.append("{");
    StringJoiner actionsString = new StringJoiner(",");
    actions.forEach(action -> actionsString.add(action.toString()));
    returnString.append(actionsString);
    returnString.append("}");
    return returnString.toString();
  }

  /**
   * Overridden equals method.
   *
   * @param o (Object) The object to be compared to.
   * @return (boolean) Returns {@code true} if the references are the same, {@code false} if not.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Link link = (Link) o;

    // This deviates from standard implementation due to the Story class using links as keys,
    // which have a passage title as both text and reference
    return reference.equals(link.reference);
  }

  /**
   * Overridden hashCode method.
   *
   * @return (int) A hash for the object.
   */
  @Override
  public int hashCode() {
    // This deviates from standard implementation due to the Story class using links as keys,
    // which have a passage title as both text and reference
    return Objects.hash(reference);
  }
}
