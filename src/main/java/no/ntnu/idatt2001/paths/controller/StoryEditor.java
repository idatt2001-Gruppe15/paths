package no.ntnu.idatt2001.paths.controller;


import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.ntnu.idatt2001.paths.components.PassageNode;
import no.ntnu.idatt2001.paths.fileio.StoryWriter;
import no.ntnu.idatt2001.paths.story.Passage;
import no.ntnu.idatt2001.paths.story.Story;

/**
 * Controller for story objects in the paths game.
 */
public final class StoryEditor {
  private static final Logger LOGGER = Logger.getLogger(StoryEditor.class.getName());

  private StoryEditor() {}

  /**
   * Writes a new story to file given the data in the parameters.
   *
   * @param title (String) The title of the story.
   * @param openingPassageNode (PassageNode) The Node representing the opening passage.
   * @param passages (List) A list containing the rest of the passages for the story.
   * @return {@code true} only if the story is written to file without issue.
   * @throws IllegalArgumentException
   *     If any of the parameters are {@code null}, or if the String or list is empty.
   */
  public static boolean createStory(
      String title,
      PassageNode openingPassageNode,
      List<PassageNode> passages) {
    StringBuilder errorMessage = new StringBuilder();
    if (title == null) {
      errorMessage.append("Title parameter cannot be null. ");
    } else if (title.isBlank()) {
      errorMessage.append("Title parameter cannot be blank. ");
    }
    if (openingPassageNode == null) {
      errorMessage.append("Opening passage parameter cannot be null. ");
    }
    if (passages == null) {
      errorMessage.append("Passages parameter cannot be null. ");
    } else if (passages.isEmpty()) {
      errorMessage.append("Passages parameter cannot be empty. ");
    }
    if (!errorMessage.isEmpty()) {
      throw new IllegalArgumentException(errorMessage.toString());
    }
    Passage openingPassage = parsePassage(Objects.requireNonNull(openingPassageNode));
    if (openingPassage == null) {
      return false;
    }
    Story story = new Story(title, openingPassage);
    Objects.requireNonNull(passages).forEach(passageNode -> {
      Passage p = parsePassage(passageNode);
      if (p != null) {
        story.addPassage(p);
      }
    });
    try {
      StoryWriter.writeToFile(story);
      return true;
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
      return false;
    }
  }

  private static Passage parsePassage(PassageNode node) {
    if (node.getTitle().isBlank() || node.getContent().isBlank()) {
      return null;
    }
    Passage passage = new Passage(node.getTitle(), node.getContent());
    if (!node.getLinks().isEmpty()) {
      node.getLinks().forEach(passage::addLink);
    }

    return passage;
  }
}
