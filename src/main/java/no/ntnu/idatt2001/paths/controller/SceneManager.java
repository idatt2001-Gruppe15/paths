package no.ntnu.idatt2001.paths.controller;

import javafx.scene.layout.Region;
import javafx.stage.Stage;
import no.ntnu.idatt2001.paths.screens.MainMenuScreen;

/**
 * Handles the changing of the root nodes of the scene.
 */
public record SceneManager(Stage stage) {
  /**
   * Constructor. Takes the current stage as parameter.
   *
   * @param stage (Stage)
   * @throws IllegalArgumentException If the parameter is {@code null}.
   */
  public SceneManager {
    if (stage == null) {
      throw new IllegalArgumentException("Scene parameter cannot be null.");
    }
  }

  /**
   * Changes the current root of the scene.
   *
   * @param root (Region) The new root object to be displayed.
   */
  public void changeScene(Region root) {
    if (root == null) {
      throw new IllegalArgumentException("Root parameter cannot be null.");
    }
    stage.getScene().setRoot(root);
  }

  /**
   * Closes the window related to this scene manager.
   */
  public void closeWindow() {
    stage.close();
  }

  public double getStageX() {
    return stage.getX();
  }

  public double getStageY() {
    return stage.getY();
  }

  /**
   * Sets the root node of the stage to the main menu screen.
   */
  public void goToMainMenu() {
    changeScene(new MainMenuScreen(this).build());
  }
}
