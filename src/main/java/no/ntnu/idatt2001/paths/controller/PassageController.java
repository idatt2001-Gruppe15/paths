package no.ntnu.idatt2001.paths.controller;

import java.util.StringJoiner;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Builder;
import no.ntnu.idatt2001.paths.components.Components;
import no.ntnu.idatt2001.paths.screens.PassageScreenBuilder;
import no.ntnu.idatt2001.paths.story.Game;
import no.ntnu.idatt2001.paths.story.Link;

/**
 * Handles passage changes and events in the "paths" game.
 */
public class PassageController implements Builder<Region> {
  private final SceneManager sceneManager;
  private final StackPane container;
  private Region currentPassage;
  private final Game game;
  private static final String HEADER = "header";

  /**
   * Constructor.
   * <p>Take the current scene manager, as well as a Game object as parameters.</p>
   *
   * @param sceneManager (SceneManager)
   * @param game (Game) The game being played.
   * @throws IllegalArgumentException
   *     If any of the parameters are {@code null}.
   */
  public PassageController(SceneManager sceneManager, Game game) {
    if (sceneManager == null) {
      throw new IllegalArgumentException("Scene manager parameter cannot be null.");
    }
    if (game == null) {
      throw new IllegalArgumentException("Game parameter cannot be null.");
    }
    this.sceneManager = sceneManager;
    this.game = game;
    currentPassage = new PassageScreenBuilder(
        this,
        game.begin(),
        this.game.getStory().getBrokenLinks()).build();
    container = new StackPane();
    container.getChildren().add(currentPassage);
  }

  @Override
  public Region build() {
    return container;
  }

  /**
   * Goes to a new passage in the story using the link given as parameter.
   *
   * @param link (Link) The link to the next passage. If this is {@code null},
   *             the end of the story is reached.
   */
  public void changePassageScreen(Link link) {
    if (link == null) {
      currentPassage = goalCompleteWindow();
    } else {
      currentPassage = new PassageScreenBuilder(
          this,
          game.go(link),
          game.getStory().getBrokenLinks()).build();

      link.getActions().forEach(action -> action.execute(game.getPlayer()));
      if (game.getPlayer().getHealth() == 0) {
        currentPassage = gameOverWindow();
      }
    }
    container.getChildren().clear();
    container.getChildren().add(currentPassage);
  }

  private Region goalCompleteWindow() {
    Label congrats = new Label("Congratulations!");
    congrats.getStyleClass().add(HEADER);
    Label achieved = new Label("You have achieved the following goal(s):");
    achieved.getStyleClass().add(HEADER);
    StringJoiner text = new StringJoiner("\n");
    game.getGoals().forEach(goal -> {
      if (goal.isFulfilled(game.getPlayer())) {
        text.add(goal + " was fulfilled.");
      } else {
        text.add(goal + " was not fulfilled.");
      }
    });
    Text goalText = new Text(text.toString());
    goalText.getStyleClass().add("content");
    StackPane goalTextContainer = new StackPane(goalText);
    goalTextContainer.getStyleClass().add("content-box");
    goalTextContainer.setMaxWidth(200);
    Button quit = Components.createButton("Quit");
    quit.setOnAction(event -> sceneManager.goToMainMenu());
    HBox buttons = new HBox(5);
    buttons.getChildren().addAll(quit);
    buttons.setAlignment(Pos.CENTER);
    VBox window = new VBox();
    window.getChildren().addAll(congrats, achieved, goalTextContainer, buttons);
    window.setAlignment(Pos.CENTER);
    return window;
  }

  private Region gameOverWindow() {
    Label gameOver = new Label("Game Over");
    gameOver.getStyleClass().add(HEADER);
    Label gameOverInfo = new Label("You have died."
                                   + "\n Press restart to try again, or quit back to main menu.");

    gameOverInfo.getStyleClass().add(HEADER);

    Button quit = Components.createButton("Quit");
    quit.setOnAction(event -> sceneManager.goToMainMenu());
    HBox buttons = new HBox(5);
    buttons.getChildren().add(quit);
    buttons.setAlignment(Pos.CENTER);
    VBox window = new VBox();
    window.getChildren().addAll(gameOver, gameOverInfo, buttons);
    window.setAlignment(Pos.CENTER);
    return window;
  }
}
